#!/bin/sh
#
# build.sh: Building FlightGear App Bundle w/ Universal Binary
# By Tatsuhiro Nishioka
#

ARCHS="ppc i386"
PREFIX=$PWD/build
BIN_DIR=$PREFIX/FlightGear.app/Contents/MacOS
FRAMEWORK_DIR=$PREFIX/FlightGear.app/Contents/Frameworks
RESOURCE_DIR=$PREFIX/FlightGear.app/Contents/Resources
SDK_PATH="/Developer/SDKs/MacOSX10.6.sdk"
OSX_TARGET="10.5"

echo "Building FlightGear.app in unix way." 
echo "Everything will be build under $PREFIX/FlightGear.app."
echo "This will take a while so be patient...."

#
# Parsing Options
#
for OPT in "$@"; do
  case $OPT in
  --prefix*)
    PREFIX=`echo $OPT | cut -d "=" -f 2`
    ;;
  --skip*)
    args=`echo "$OPT" | ruby -e "print readline.split('=').last.split(',').join(' ')"`
    for i in $args; do
      case $i in
        'plib')
          SKIP_PLIB=YES
          ;;
        'simgear'|'sg')
          SKIP_SG=YES
          ;;
        'osg')
          SKIP_OSG=YES
          ;;
        'libs')
          SKIP_LIBS=YES
          ;;
        'flightgear'|'fg')
          SKIP_FG=YES
          ;;
        'fgcom')
          SKIP_FGCOM=YES
          ;;
        'atlas')
          SKIP_ATLAS=YES
          ;;
      esac
    done
    ;;
  -d | --debug)
    FLAGS="--debug"
    ;;
  --archs*)
    ARCHS=`echo $OPT | cut -d "=" -f 2`
    ;;
  *)
    echo "Unknown option $OPT"
    exit
  esac
done


#
# PLIB
#
if [ "$SKIP_PLIB" != "YES" ]; then
  for ARCH in $ARCHS ; do
    ./build-one-arch.sh --valid-archs="$ARCHS" --arch=$ARCH --skip=libs,osg,sg,fg $FLAGS || exit
  done
fi

#
# OpenSceneGraph
#
if [ "$SKIP_OSG" != "YES" ]; then
  for ARCH in $ARCHS ; do
    ./build-one-arch.sh --valid-archs="$ARCHS" --arch=$ARCH --skip=libs,plib,sg,fg $FLAGS || exit
  done
fi

#
# SimGear
#
if [ "$SKIP_SG" != "YES" ]; then
  for ARCH in $ARCHS ; do
    ./build-one-arch.sh --valid-archs="$ARCHS" --arch=$ARCH --skip=libs,fg $FLAGS --with-plib-frameworks=$PWD/build/PLIB --with-osg-frameworks=$PWD/build/OpenSceneGraph || exit
  done
fi

#
# FlightGear 
#
if [ "$SKIP_FG" != "YES" ]; then
  for ARCH in $ARCHS ; do
    ./build-one-arch.sh --valid-archs="$ARCHS" --arch=$ARCH --skip=libs,sg $FLAGS --with-plib-frameworks=$PWD/build/PLIB --with-osg-frameworks=$PWD/build/OpenSceneGraph || exit
  done
fi

#
# fgcom
#
if [ "$SKIP_FGCOM" != "YES" ]; then
  BUILD_DIR=$PWD/fgcom
  TARGET_DIR=$PWD/build/fgcom
  pushd $BUILD_DIR/src
  [ -f getopt.c ] && rm -rf getopt.[ch]
  echo "Building fgcom"
  make
  
  mkdir -p $TARGET_DIR/data/fgcom
  cp $BUILD_DIR/src/fgcom $TARGET_DIR
  cp $BUILD_DIR/data/*.txt $TARGET_DIR/data/fgcom
  popd
  echo "fgcom is built in $TARGET_DIR"
fi

#
# Atlas
#
set -x
if [ "$SKIP_ATLAS" != "YES" ]; then
  # configure 
  echo "configuring Atlas"
  pushd Atlas

  VER="2.5.0"
  TARGET_DIR=$PWD/../build/Atlas
  SDKROOT=/Developer/SDKs/MacOSX10.6.SDK
  _CC="gcc-4.2"
  _CXX="g++-4.2"
  _CFLAGS="-isysroot $SDKROOT -mmacosx-version-min=10.5 -O2 -I$PWD/libjpeg-6b"

  ./autogen.sh
  CXXFLAGS="-I/usr/local/include" \
  LDFLAGS="-L$PWD/libjpeg-6b" \
  ./configure --with-osg=$PWD/../OpenSceneGraph --with-plib-framework=$PWD/../build/PLIB \
    --with-simgear=$PWD/../build/SimGear --with-fgbase=/Applications/FlightGear.app/Contents/Resources/data 
    --with-libcurl=$PWD/Libs/curl-7.19.7

  # make
  cd src
  echo cleaning up

  make clean
  rm -rf arch

  mkdir -p arch/ppc
  mkdir -p arch/i386

  echo Making Atlas binaries for PPC 
  rm -rf *.o Map Atlas GetMap
  CC="$_CC -arch ppc $_CFLAGS" CXX="$_CXX -arch ppc $_CFLAGS" make -e -j 4
  mv Map Atlas GetMap arch/ppc

  echo Making Atlas binaries for i386
  rm -rf *.o Map Atlas GetMap
  CC="$_CC -arch i386 $_CFLAGS" CXX="$_CXX -arch i386 $_CFLAGS" make -e -j 4
  mv Map Atlas GetMap arch/i386

  echo Making universal binaries
  for i in arch/i386/*; do
    echo "  `basename $i`"
    lipo -create arch/i386/`basename $i` arch/ppc/`basename $i` -output `basename $i`
  done

  # copy binaries
  mkdir -p $TARGET_DIR/data/Atlas
  cp Map Atlas GetMap $TARGET_DIR
  rsync -ar --exclude=CVS --exclude=.cvsignore --exclude="Makefile*" data/* $TARGET_DIR/data/Atlas/

  echo "Atlas is built in $TARGET_DIR"t
  popd
fi
#
# Making Application Bundle
#

pushd FlightGearOSX
echo "Making App Bundle..."
rm -rf $PREFIX/FlightGear.app

# building ruby cocoa main program
gcc -o FlightGear -mmacosx-version-min=$OSX_TARGET -isysroot $SDK_PATH -arch ppc -arch i386 main.m -framework Cocoa -framework RubyCocoa -framework Foundation -framework AppKit
popd

mkdir -p $BIN_DIR
mkdir -p $FRAMEWORK_DIR
mkdir -p $RESOURCE_DIR/plugins

# copying resources
pushd FlightGearOSX
rsync -a --exclude=".svn" *.rb *.tiff *.html *.sh *.lproj *.icns $RESOURCE_DIR
cp FlightGear $BIN_DIR || exit
ln -s $PWD/data $RESOURCE_DIR/data || exit

# copying Info.plist and InfoPlist.strings
REV=`svnversion .. | cut -d ":" -f 2`
REV=${REV%M}
YEAR=`date +"%Y"`
#VER="git-`date +'%Y%m%d'`"
VER="2.5.0"
cat Info-FlightGear.plist.in | sed "s/%BUILD_NUMBER%/$REV/" | sed "s/%VERSION%/$VER/" > $RESOURCE_DIR/../Info.plist
cat English.lproj/InfoPlist.strings.in | sed "s/%BUILD_NUMBER%/$REV/" | sed "s/%VERSION%/$VER/" | sed "s/%YEAR%/$YEAR/" | iconv -f ascii -t utf-16 > $RESOURCE_DIR/English.lproj/InfoPlist.strings
rm $RESOURCE_DIR/English.lproj/*.in
popd
. credits.sh > $RESOURCE_DIR/Credits.html

# copying frameworks
cp -R $PREFIX/OpenSceneGraph/*.framework $FRAMEWORK_DIR || exit
cp -R $PREFIX/PLIB/PLIB.framework $FRAMEWORK_DIR || exit
cp -R /Library/Frameworks/ALUT.framework $FRAMEWORK_DIR || exit
# cp -R /Library/Frameworks/RubyCocoa.framework $FRAMEWORK_DIR || exit
cp $PREFIX/OpenSceneGraph/osgPlugins*/*.so $RESOURCE_DIR/plugins || exit
cp $PREFIX/FlightGear/bin/* $RESOURCE_DIR/ || exit
cp -R $PREFIX/fgcom/* $RESOURCE_DIR/ || exit
cp -R $PREFIX/Atlas/* $RESOURCE_DIR/ || exit

# ALUT's install_name is sometimes wrong ($HOME/Library/Frameworks/ALUT.framework/Versions/A/ALUT, 
# but should begin with @executable_path)
install_name_tool -change $HOME/Library/Frameworks/ALUT.framework/Versions/A/ALUT \
                  @executable_path/../Frameworks/ALUT.framework/Versions/A/ALUT \
                  $RESOURCE_DIR/fgfs || exit

echo "FlightGear.app is built on $PREFIX folder."
echo "You can copy it to /Applications."
echo "NOTICE: data folder under FlightGear.app is a symbolic link to FlightGearOSX/data. "
echo "NOTICE: So updating the data using download.sh will effect/affect the data under the app."
