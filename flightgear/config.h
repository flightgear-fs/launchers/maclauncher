/* src/Include/config.h.  Generated from config.h.in by configure.  */
/* src/Include/config.h.in.  Generated from configure.ac by autoheader.  */

/* define if OSG doesn't have CullSettings::CLEAR_MASK */
/* #undef DONT_HAVE_CULLSETTINGS_CLEAR_MASK */

/* Define for audio support */
#define ENABLE_AUDIO_SUPPORT 1

/* Define to enable plib joystick support */
#define ENABLE_PLIB_JOYSTICK 1

/* Define to include special purpose FDMs */
#define ENABLE_SP_FDM 1

/* Define to enable threaded tile paging */
#define ENABLE_THREADS 1

/* Define so that JSBSim compiles in 'library' mode */
#define FGFS 1

/* Define to build with jpeg screen shot server */
/* #undef FG_JPEG_SERVER */

/* Define for no logging output */
/* #undef FG_NDEBUG */

/* Define for fxmesa */
/* #undef FX */

/* Define to 1 if you have the `bcopy' function. */
#define HAVE_BCOPY 1

/* define if the Boost library is available */
#define HAVE_BOOST 

/* define if OSG has CullSettings::CLEAR_MASK */
/* #undef HAVE_CULLSETTINGS_CLEAR_MASK */

/* Define if system has daylight variable */
#define HAVE_DAYLIGHT 1

/* Define to 1 if you don't have `vprintf' but do have `_doprnt.' */
/* #undef HAVE_DOPRNT */

/* Define to 1 if you have the `drand48' function. */
#define HAVE_DRAND48 1

/* Define to 1 if you have the <fcntl.h> header file. */
#define HAVE_FCNTL_H 1

/* define if system has fenableexcept */
/* #undef HAVE_FEENABLEEXCEPT */

/* Define if framework OpenThreads is available. */
#define HAVE_FRAMEWORK_OPENTHREADS 1

/* Define if framework osg is available. */
#define HAVE_FRAMEWORK_OSG 1

/* Define if framework osgDB is available. */
#define HAVE_FRAMEWORK_OSGDB 1

/* Define if framework osgFX is available. */
#define HAVE_FRAMEWORK_OSGFX 1

/* Define if framework osgGA is available. */
#define HAVE_FRAMEWORK_OSGGA 1

/* Define if framework osgParticle is available. */
#define HAVE_FRAMEWORK_OSGPARTICLE 1

/* Define if framework osgSim is available. */
#define HAVE_FRAMEWORK_OSGSIM 1

/* Define if framework osgText is available. */
#define HAVE_FRAMEWORK_OSGTEXT 1

/* Define if framework osgUtil is available. */
#define HAVE_FRAMEWORK_OSGUTIL 1

/* Define if framework osgViewer is available. */
#define HAVE_FRAMEWORK_OSGVIEWER 1

/* Define if framework PLIB is available. */
/* #undef HAVE_FRAMEWORK_PLIB */

/* Define to 1 if you have the `ftime' function. */
#define HAVE_FTIME 1

/* Define to 1 if you have the `getitimer' function. */
#define HAVE_GETITIMER 1

/* Define to 1 if you have the `GetLocalTime' function. */
/* #undef HAVE_GETLOCALTIME */

/* Define to 1 if you have the <getopt.h> header file. */
#define HAVE_GETOPT_H 1

/* Define to 1 if you have the `getrusage' function. */
#define HAVE_GETRUSAGE 1

/* Define to 1 if you have the `gettimeofday' function. */
#define HAVE_GETTIMEOFDAY 1

/* Define to 1 if you have the <glut.h> header file. */
/* #undef HAVE_GLUT_H */

/* Define to 1 if you have the <inttypes.h> header file. */
#define HAVE_INTTYPES_H 1

/* Define to 1 if you have the `jpeg' library (-ljpeg). */
/* #undef HAVE_LIBJPEG */

/* Define to 1 if you have the `OpenThreads' library (-lOpenThreads). */
/* #undef HAVE_LIBOPENTHREADS */

/* Define to 1 if you have the `OpenThreadsd' library (-lOpenThreadsd). */
/* #undef HAVE_LIBOPENTHREADSD */

/* Define to 1 if you have the `osg' library (-losg). */
/* #undef HAVE_LIBOSG */

/* Define to 1 if you have the `osgd' library (-losgd). */
/* #undef HAVE_LIBOSGD */

/* Define to 1 if you have the `osgDB' library (-losgDB). */
/* #undef HAVE_LIBOSGDB */

/* Define to 1 if you have the `osgDBd' library (-losgDBd). */
/* #undef HAVE_LIBOSGDBD */

/* Define to 1 if you have the `osgGA' library (-losgGA). */
/* #undef HAVE_LIBOSGGA */

/* Define to 1 if you have the `osgGAd' library (-losgGAd). */
/* #undef HAVE_LIBOSGGAD */

/* Define to 1 if you have the `osgParticle' library (-losgParticle). */
/* #undef HAVE_LIBOSGPARTICLE */

/* Define to 1 if you have the `osgParticled' library (-losgParticled). */
/* #undef HAVE_LIBOSGPARTICLED */

/* Define to 1 if you have the `osgSim' library (-losgSim). */
/* #undef HAVE_LIBOSGSIM */

/* Define to 1 if you have the `osgSimd' library (-losgSimd). */
/* #undef HAVE_LIBOSGSIMD */

/* Define to 1 if you have the `osgText' library (-losgText). */
/* #undef HAVE_LIBOSGTEXT */

/* Define to 1 if you have the `osgTextd' library (-losgTextd). */
/* #undef HAVE_LIBOSGTEXTD */

/* Define to 1 if you have the `osgUtil' library (-losgUtil). */
/* #undef HAVE_LIBOSGUTIL */

/* Define to 1 if you have the `osgUtild' library (-losgUtild). */
/* #undef HAVE_LIBOSGUTILD */

/* Define to 1 if you have the `osgViewer' library (-losgViewer). */
/* #undef HAVE_LIBOSGVIEWER */

/* Define to 1 if you have the `osgViewerd' library (-losgViewerd). */
/* #undef HAVE_LIBOSGVIEWERD */

/* Define to 1 if you have the `svn_client-1' library (-lsvn_client-1). */
#define HAVE_LIBSVN_CLIENT_1 1

/* Define to 1 if you have the <malloc.h> header file. */
/* #undef HAVE_MALLOC_H */

/* Define to 1 if you have the `memcpy' function. */
#define HAVE_MEMCPY 1

/* Define to 1 if you have the <memory.h> header file. */
#define HAVE_MEMORY_H 1

/* Define to 1 if you have the `mkfifo' function. */
#define HAVE_MKFIFO 1

/* Define to 1 if you have the `mktime' function. */
#define HAVE_MKTIME 1

/* Define to 1 if you have the `rand' function. */
#define HAVE_RAND 1

/* Define to 1 if you have the `random' function. */
#define HAVE_RANDOM 1

/* Define to 1 if you have the `rint' function. */
#define HAVE_RINT 1

/* Define to 1 if you have the `setitimer' function. */
#define HAVE_SETITIMER 1

/* Define to 1 if you have the `signal' function. */
#define HAVE_SIGNAL 1

/* Define to 1 if you have the <stdint.h> header file. */
#define HAVE_STDINT_H 1

/* Define to 1 if you have the <stdlib.h> header file. */
#define HAVE_STDLIB_H 1

/* Define to 1 if you have the <strings.h> header file. */
#define HAVE_STRINGS_H 1

/* Define to 1 if you have the <string.h> header file. */
#define HAVE_STRING_H 1

/* Define to 1 if you have the `strstr' function. */
#define HAVE_STRSTR 1

/* Define to 1 if you have the <svn_client.h> header file. */
#define HAVE_SVN_CLIENT_H 1

/* Define to 1 if you have the <sys/param.h> header file. */
#define HAVE_SYS_PARAM_H 1

/* Define to 1 if you have the <sys/stat.h> header file. */
#define HAVE_SYS_STAT_H 1

/* Define to 1 if you have the <sys/timeb.h> header file. */
#define HAVE_SYS_TIMEB_H 1

/* Define to 1 if you have the <sys/time.h> header file. */
#define HAVE_SYS_TIME_H 1

/* Define to 1 if you have the <sys/types.h> header file. */
#define HAVE_SYS_TYPES_H 1

/* Define to 1 if you have the `timegm' function. */
#define HAVE_TIMEGM 1

/* Define if system has timezone variable */
#define HAVE_TIMEZONE 1

/* Define to 1 if you have the `truncf' function. */
#define HAVE_TRUNCF 1

/* Define to 1 if you have the <unistd.h> header file. */
#define HAVE_UNISTD_H 1

/* Define to 1 if you have the <values.h> header file. */
/* #undef HAVE_VALUES_H */

/* Define to 1 if you have the `vprintf' function. */
#define HAVE_VPRINTF 1

/* Define to 1 if you have the <windows.h> header file. */
/* #undef HAVE_WINDOWS_H */

/* Define for Win32 platforms */
/* #undef NOMINMAX */

/* Name of package */
#define PACKAGE "FlightGear"

/* Define to the address where bug reports for this package should be sent. */
#define PACKAGE_BUGREPORT ""

/* Define to the full name of this package. */
#define PACKAGE_NAME ""

/* Define to the full name and version of this package. */
#define PACKAGE_STRING ""

/* Define to the one symbol short name of this package. */
#define PACKAGE_TARNAME ""

/* Define to the version of this package. */
#define PACKAGE_VERSION ""

/* Define to use application's pu callbacks */
#define PU_USE_NONE 1

/* Define as the return type of signal handlers (`int' or `void'). */
#define RETSIGTYPE void

/* Define to 1 if you have the ANSI C header files. */
#define STDC_HEADERS 1

/* Define to 1 if you can safely include both <sys/time.h> and <time.h>. */
#define TIME_WITH_SYS_TIME 1

/* Define to 1 if your <sys/time.h> declares `struct tm'. */
/* #undef TM_IN_SYS_TIME */

/* Version number of package */
#define VERSION "1.9.1"

/* Define for Win32 platforms */
/* #undef WIN32 */

/* Define to enable generic event driven input device */
#define WITH_EVENTINPUT 1

/* Define for fxmesa */
/* #undef XMESA */

/* Define to 1 if the X Window System is missing or not being used. */
/* #undef X_DISPLAY_MISSING */

/* Define to empty if `const' does not conform to ANSI C. */
/* #undef const */

/* Define to `unsigned int' if <sys/types.h> does not define. */
/* #undef size_t */
