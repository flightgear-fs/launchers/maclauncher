#!/bin/sh
#
# Usage: patch.sh [extra options]
# You can specify --dry-run as an extra option to dry run the patch.

if [ ! -d PLIB/plib ]; then
  . ./download.sh
fi

files=`ls patches/*.diff`
for i in $files; do
  patch -N $1 -p0 < $i
done

echo "done. now ready to build."

