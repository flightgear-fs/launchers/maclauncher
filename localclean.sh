#
# This script will clean up everything you built with localbuild.sh
#

for i in PLIB/plib Simgear/SimGear flightgear/FlightGear; do
  pushd $i
  make clean
  popd
done

rm -rf OpenSceneGraph/Xcode/OpenSceneGraph/build
#rm -rf libs/build
