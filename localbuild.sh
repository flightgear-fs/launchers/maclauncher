#
# This script is for building fgfs for your local end (with Xcode 3.1 or greater)
# Be aware that this doesn't provide the GUI launcher, but generates everything that
# FlightGear's Makefile does. 
# You need to run build.sh instead of this script if you want to build an OS X application
#
# fgfs will be made at /usr/local/FlightGear/bin by default
# default destination is /usr/local/FlightGear
# 
# usage: localbuild.sh [--skip={download,patch,plib,simgear,osg,libs,flightgear}]
# e.g. localbuild.sh --skip=download,patch,plib,osg # begins from simgear build
# 
# Building steps
# 1) download everything you need
# 2) apply patches
# 3) build PLIB and install it to /usr/local/PLIB (password required)
# 4) build OpenSceneGraph (Frameworks are installed to /Library/Frameworks)
#    plugins will be installed to /usr/local/OpenSceneGraph/lib
# 5) build Simgear and install it to /usr/local/Simgear
# 6) build Libraries required for TerraSync (I hate this...)
# 7) build FlightGear and install it to /usr/local/FlightGear

# Prerequisition:
# - You must have OS X 10.5 or later
# - You must have XCode 3.1 or later
# - You must apply patch to /Developer/SDKs/MacOSX10.5.sdk/Systems/Library/Framework/OpenAL.framework/Headers/alc.h
#   to avoid compilation error. This is a clear bug in OpenAL, but Apple haven't fixed it yet even I complained about it a while ago.
#   (Original OpenAL already fixed this issue.)
# - You must have enough time to build everything. It may take a while :-p

set -ex

echo "Building fgfs in unix way..... No GUI launcher will be build"

#
# Parsing argument
# --skip=download,patch,plib,simgear will skip building PLIB and SimGear
#
PREFIX="/usr/local"
ARCH="i386"

for OPT in "$@"; do
  case $OPT in
  --prefix*)
    PREFIX=`echo $OPT | cut -d "=" -f 2`
    ;;
  --skip*)
    args=`echo "$OPT" | ruby -e "print readline.split('=').last.split(',').join(' ')"`
    for i in $args; do
      case $i in
        'download')
          SKIP_DOWNLOAD=YES
          ;;
        'patch')
          SKIP_PATCH=YES
          ;;
        'plib')
          SKIP_PLIB=YES
          ;;
        'simgear'|'sg')
          SKIP_SIMGEAR=YES
          ;;
        'osg')
          SKIP_OSG=YES
          ;;
       'libs')
          SKIP_LIBS=YES
          ;;
        'flightgear'|'fg')
          SKIP_FG=YES
          ;;
      esac
    done
    ;;
  -d | --debug)
    DEBUG=YES
    ;;
  --arch*)
    ARCH=`echo $OPT | cut -d "=" -f 2`
    ;;
  --with-plib-framework*)
    PLIB_FRAMEWORK_DIR=`echo $OPT | cut -d "=" -f 2`
    SKIP_PLIB=YES
    ;;
  --with-osg=*)
    OSG_LIB_DIR=`echo $OPT | cut -d "=" -f 2`
    SKIP_OSG=YES
    ;;
  --with-osg-framework*)
    OSG_FRAMEWORK_DIR=`echo $OPT | cut -d "=" -f 2`
    SKIP_OSG=YES
    ;;
  --with-openal-framework*)
    OPENAL_FRAMEWORK_DIR=`echo $OPT | cut -d "=" -f 2`
    ;;
  *)
    echo "Unknown option $OPT"
    exit
  esac
done

# 
#
# Configuration
# 
#

# Setting Architecture specific options
if [ "$ARCH" == "ppc" ]; then
  _CXXFLAGS="$OPT_PPC $_CXXFLAGS"
elif [ "$ARCH" == "x86_64" ]; then
  _CXXFLAGS="$OPT_I386 $_CXXFLAGS" 
else
  ARCH=i386
  _CXXFLAGS="$OPT_I386 $_CXXFLAGS" 
fi

# Destinations
FG_DIR="$PREFIX/FlightGear"
BOOST_DIR="$PWD/boost_1_41_0"
PLIB_DIR="$PREFIX/PLIB"
SG_DIR="$PREFIX/SimGear"
OSG_PLUGIN_DIR="$PREFIX/OpenSceneGraph/lib"

# Framework Paths (specify only if you want to reuse existing frameworks)
# if [ ! -z "$PLIB_FRAMEWORK_DIR" ]; then
#  PLIB_FRAMEWORK_DIR="$PWD/PLIB/build/Release"
# fi

if [ -z "$OSG_FRAMEWORK_DIR$OSG_LIB_DIR" ]; then
  OSG_FRAMEWORK_DIR="$PWD/OpenSceneGraph/Xcode/OpenSceneGraph/build/Deployment"
fi

# build settings
OSX_TARGET="10.4"
SDK_PATH="/Developer/SDKs/MacOSX10.5.sdk"
_CC="/Developer/usr/bin/gcc-4.2"
_CXX="/Developer/usr/bin/g++-4.2"
RELEASE_FLAGS="-O$OPT_LEVEL"
DEBUG_FLAGS="-O0 -g"

# per architecture buid settings
OPT_I386="-arch i386 -march=prescott -mfpmath=sse -msse3"
OPT_PPC="-arch ppc -mtune=G4"

_CXXFLAGS="-fmessage-length=0 -pipe -fpascal-strings -fasm-blocks -fpermissive -mmacosx-version-min=$OSX_TARGET -isysroot $SDK_PATH"
_LDFLAGS="-arch $ARCH -L$PWD/libs/build/lib"

# Setting Debug / Release flags
if [ -z "$DEBUG" ]; then
  BUILD_FLAGS=$RELEASE_FLAGS
  OPT_LEVEL=3
else
  BUILD_FLAGS=$DEBUG_FLAGS
  OPT_LEVEL=0
fi

# Determine if you need PLIB.framework or static libs
if [ ! -z "$PLIB_FRAMEWORK_DIR" ]; then
  PLIB_OPTION="--with-plib-framework=$PLIB_FRAMEWORK_DIR"
else
  PLIB_OPTION="--with-plib=$PREFIX/PLIB"
fi

if [ ! -z "$OSG_LIB_DIR" ]; then
  OSG_OPTION="--with-osg=$OSG_LIB_DIR"
else
  OSG_OPTION="--with-osg-framework=$OSG_FRAMEWORK_DIR"
fi

if [ ! -z "$OPENAL_FRAMEWORK_DIR" ]; then
  OPENAL_OPTION="--with-openal-framework=$OPENAL_FRAMEWORK_DIR"
fi

#
#
# Building process
#
#

# Downloading all sources
if [ -z "$SKIP_DOWNLOAD" ]; then
  ./download.sh
fi

# Applying patches
if [ -z "$SKIP_PATCH" ]; then
  ./patch.sh
  sudo patch -p0 -N < patches/alc.patch
fi

#
# PLIB
#
# This is for building static libraries.
# If you want PLIB.framework, then build it with xcode and then
# specify --with-plib-framework option
if [ -z "$SKIP_PLIB" ]; then
  pushd PLIB/plib
  if [ ! -f configure ]; then
    ./autogen.sh
  fi
  CXXFLAGS="$BUILD_FLAGS $_CXXFLAGS" \
  CFLAGS="$BUILD_FLAGS $_CXXFLAGS" \
  CC="$_CC" CXX="$_CXX" \
  LDFLAGS="-arch $ARCH" \
  ./configure --prefix="$PLIB_DIR" --disable-sl || exit

  make -e -j 8 || exit
  sudo make install
  popd
fi

#
# OpenSceneGraph
#
if [ -z "$SKIP_OSG" ]; then
  #
  # building OSG using xcodebuild.
  #
  pushd OpenSceneGraph
  PROJ=Xcode/OpenSceneGraph/OpenSceneGraph.xcodeproj
  TARGET_ARCHS=$ARCH
  SDKPATH="$SDK_PATH"
  ARCH_OPT_i386="-msse3 -march=prescott -mfpmath=sse"
  CCFLAGS="\
  GCC_ENABLE_SSE3_EXTENSIONS=YES \
  GCC_INLINES_ARE_PRIVATE_EXTERN=NO \
  GCC_ENABLE_SSE3_EXTENSIONS=YES \
  GCC_MODEL_TUNING=G4 \
  GCC_INLINES_ARE_PRIVATE_EXTERN=NO \
  GCC_SYMBOLS_PRIVATE_EXTERN=NO \
  MAXOSX_DEPLOYMENT_TARGET=$OSX_TARGET \
  GCC_GENERATE_DEBUGGING_SYMBOLS=$DEBUG \
  GCC_OPTIMIZATION_LEVEL=$OPT_LEVEL \
  GCC_VERSION=4.2 \
  SDKROOT=$SDKPATH \
  "
  xcodebuild -target osgFrameworks -configuration Deployment -project $PROJ ARCHS="$TARGET_ARCHS" $CCFLAGS PER_ARCH_CFLAGS_i386="$ARCH_OPT_i386" || exit
  xcodebuild -target osgPlugins -configuration Deployment -project $PROJ ARCHS="$TARGET_ARCHS" $CCFLAGS PER_ARCH_CFLAGS_i386="$ARCH_OPT_i386" || exit

  # installing OSG Frameworks
  # this is needed for conifgure in both SimGear and FlightGear
  for i in $OSG_FRAMEWORK_DIR/*.framework; do
    if [ -d  /Library/Frameworks/`basename $i` ]; then
      rm -rf /Library/Frameworks/`basename $i`
      cp -R $i /Library/Frameworks/
    fi
  done
  # installing plugins
  sudo mkdir -p $OSG_PLUGIN_DIR
  for i in $PWD/Xcode/OpenSceneGraph/build/Deployment/*.so; do
    sudo cp $i $OSG_PLUGIN_DIR
  done
  popd
fi

#
# SimGear
#
if [ -z "$SKIP_SIMGEAR" ]; then
  pushd SimGear/SimGear
  if [ ! -f configure ]; then
    ./autogen.sh
  fi
  CXXFLAGS="-I$BOOST_DIR -arch $ARCH -Wall -DNDEBUG -D_REENTRANT -DHAVE_CONFIG_H $BUILD_FLAGS $_CXXFLAGS" \
  CFLAGS="-I$BOOST_DIR -arch $ARCH -Wall -DNDEBUG -D_REENTRANT -DHAVE_CONFIG_H $BUILD_FLAGS $_CXXFLAGS" \
  CC="$_CC" CXX="$_CXX" \
  ./configure --prefix="$SG_DIR" $PLIB_OPTION $OSG_OPTION $OPENAL_OPTION \
    --with-boost="$BOOST_DIR" --with-boost-libdir="$BOOST_DIR" \
    --with-alut-framework=/Library/Frameworks \
    --with-jpeg-server=no
  make -j 8
  sudo make install
#  sudo cp simgear/sound/alut.h $SG_DIR/include/simgear/sound/
  sudo rm -rf $SG_DIR/include/simgear/screen/jpgfactory.hxx
  popd
fi

#
# Libraries for Terrasync
#
if [ -z "$SKIP_LIBS" ]; then
  pushd libs
  ./build.sh
  popd
fi

#
# FlightGear
#
if [ -z "$SKIP_FG" ]; then
  pushd flightgear/FlightGear
  if [ ! -f configure ]; then
    ./autogen.sh
  fi
  CXXFLAGS="-I$BOOST_DIR -Wall -DENABLE_AUDIO_SUPPORT -DHAVE_CONFIG_H $BUILD_FLAGS $_CXXFLAGS" \
  CFLAGS="$CXXFLAGS" \
  CC="$_CC -arch $ARCH" CXX="$_CXX -arch $ARCH" LDFLAGS="$_LDFLAGS"  \
  ./configure --prefix="$FG_DIR" $PLIB_OPTION $OSG_OPTION --with-simgear="$SG_DIR" $OPENAL_OPTION \
  --with-boost="$BOOST_DIR" --with-boost-libdir="$BOOST_DIR" --enable-osgviewer --with-eventinput \
  --with-alut-framework=/Library/Frameworks

  # FG's configure enables JPEG SERVER even your Mac doesn't have libjpeg, so force disable it.
  # (This is because Mac's gcc accepts (or ignores?) -ljpeg even you don't have libjpeg.a, and thus FG's configure believe there exists libjpeg)
  # You can cut the next line if you know your Mac has libjpeg
  cp src/Include/config.h /tmp; cat /tmp/config.h | sed 's!#define FG_JPEG_SERVER 1!/* #under FG_JPEG_SERVER */!' > src/Include/config.h
  make -j 8
  sudo make install
  popd
  if [ ! -L $FG_DIR/data ]; then
    sudo ln -s $PWD/FlightGearOSX/data $FG_DIR/data
  fi
fi

echo Done. fgfs is made at $FG_DIR
echo You need to export DYLD_LIBRARY_PATH=$OSG_PLUGIN_DIR before running fgfs
echo You also need to specify --fg-root=... to fgfs
echo Good luck!
