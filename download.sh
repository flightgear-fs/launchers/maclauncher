#!/bin/sh

# svn path for general/darwin-ports/fink 
# PATH=$PATH:/usr/local/bin:/opt/local/bin:/sw/bin

if [ ! -z "$1" ]; then
  FG_USER=$1
else
  FG_USER="cvsguest"
fi

download()
{
# name, url, dest
  if [ ! -e `basename $2` ]; then
    echo "downloading $1 ..."
    curl -O $2
  fi
  echo "extracting $1 ..."
  if [ ! -z `echo $2 | grep "gz"` ]; then
    tar xzf `basename $2` -C $3
  elif [ ! -z `echo $2 | grep "dmg"` ]; then
    hdiutil attach `basename $2`
  else
    # assume bz2
    tar xjf `basename $2` -C $3
  fi
}

checkout_svn()
{
# name, url, repository, dest
  pushd ./
  if [ ! -d "$4" ]; then
    echo "checking out $1 from svn..."
    dest=`dirname $4`
    mkdir -p $4
    cd $dest
    LC_ALL=C svn co $2/$3 `basename $4`
  else
    echo "updating $1 from svn..."
    cd $4
    LC_ALL=C svn update
  fi
  popd
}
  
checkout_cvs()
{
# name, cvsroot, repository, dest, passwd
  if [ ! -z "$5" ]; then
    echo type "$5" as cvspassword
  else
    echo type 'guest' or blank as cvspassword
  fi
  pushd ./
  if [ ! -d "$4" ]; then
    echo "checking out $1 from cvs..."
    dest=`dirname $4`
    mkdir -p $4
    cd $dest
    cvs -d $2 login
    cvs -d $2 co -d `basename $4` $3
  else
    echo "updating $1 from cvs..."
    cd $4
    cvs update -Pd
  fi
  popd
}

checkout_git()
{
# name url dest [tag]
  if [ ! -d "$3" ]; then
    echo "cloning $1 via $2"
    git clone $2 "$3"
    if [ ! -z $4 ]; then
      pushd .
      cd "$3"
      git checkout $4
      popd
    fi
  else 
    echo "updating $1"
    pushd .
    cd "$3"
    git pull 
    popd
  fi
}

# PLIB
##checkout_svn "PLIB" "https://plib.svn.sourceforge.net/svnroot/plib" "trunk" "PLIB/plib"

# SimGear
##checkout_git "SimGear" "git://gitorious.org/fg/simgear.git" "SimGear/SimGear" "next" 

# FlightGear source and data
##checkout_git "FlightGear source" "git://gitorious.org/fg/flightgear.git" "flightGear/FlightGear" "next"

#rm -rf FlightGearOSX/data
checkout_git "FlightGear base package" "git://gitorious.org/fg/fgdata.git" "FlightGearOSX/data" "next"

# OpenSceneGraph
##checkout_svn "OpenSceneGraph" "http://www.openscenegraph.org/svn/osg/OpenSceneGraph" "tags/OpenSceneGraph-3.0.1" "OpenSceneGraph" "[hit return]"

# RubyCocoa - Not needed anymore since OS X 10.5 or later has one

# Boost (needs headers at this moment)
##checkout_svn "Boost Library" "http://svn.boost.org/svn/boost/tags/release" "Boost_1_48_0/boost" "boost_1_48_0/boost"

# fgcom (rev.260 is used for v2.4.0)
##checkout_svn "fgcom" "-r 260 https://appfgcom.svn.sourceforge.net/svnroot/fgcom" "trunk" "fgcom"

# Atlas
##checkout_cvs "-D 8/16/11 :pserver:anonymous@atlas.cvs.sourceforge.net:/cvsroot" "atlas" "Atlas"
##download "prebuilt libjpeg" "http://macflightgear.sourceforge.net/wp-content/uploads/libs/libjpeg-6b.tar.gz" "Atlas"

# libjpg-6b

# prebuilt svn libs for terrasync
# You need MacOS 10.4.x for building svn libraries that are compatible with Mac OS 10.4, so I made prebuilt libs
# download "prebuilt svn libraries" "http://macflightgear.sourceforge.net/wp-content/uploads/terrasync-libs-20090718.tar.gz" "libs"

download "prebuilt ALUT framework" "http://macflightgear.sourceforge.net/wp-content/uploads/ALUT.framework.tar.gz" "/Library/Frameworks"

echo "done. Now execute patch.sh, and then build.sh" 

