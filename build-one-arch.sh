#
# Building FlightGear for a given architecture
# by Tatsuhiro Nishioka
#
# This script must be called from build.sh, and must not be directly executed by users.
#
# Prerequisition:
# - You must have OS X 10.6 or later
# - You must have XCode 4 or later
# - You must have cmake 2.6 or later (for OSG)
# - You must have enough time to build everything. It does take a while :-p

#
# Default Target architectures
#
ARCHS="ppc i386"

if [ -z "$XCODE_ROOT" ]; then 
  XCODE_ROOT="/Developer"
  GCC_PATH=$XCODE_ROOT/usr/bin
fi


#
# Parsing argument
# --skip=download,patch,plib,simgear will skip building PLIB and SimGear
#
# PREFIX="/usr/local"
BUILD_ROOT=$PWD
PREFIX=$PWD/build
mkdir -p $PWD/build
ARCH="i386"
TARGET_VERSION="2.5.0"

for OPT in "$@"; do
  case $OPT in
  --prefix*)
    PREFIX=`echo $OPT | cut -d "=" -f 2`
    ;;
  --skip*)
    args=`echo "$OPT" | ruby -e "print readline.split('=').last.split(',').join(' ')"`
    for i in $args; do
      case $i in
        'plib')
          SKIP_PLIB=YES
          ;;
        'simgear'|'sg')
          SKIP_SIMGEAR=YES
          ;;
        'osg')
          SKIP_OSG=YES
          ;;
       'libs')
          SKIP_LIBS=YES
          ;;
        'flightgear'|'fg')
          SKIP_FG=YES
          ;;
      esac
    done
    ;;
  -d | --debug)
    DEBUG=YES
    ;;
  --valid-archs*)
    ARCHS=`echo $OPT | cut -d "=" -f 2`
    ;;
  --arch*)
    ARCH=`echo $OPT | cut -d "=" -f 2`
    ;;
  --with-plib-framework*)
    PLIB_FRAMEWORK_DIR=`echo $OPT | cut -d "=" -f 2`
    SKIP_PLIB=YES
    ;;
  --with-osg=*)
    OSG_LIB_DIR=`echo $OPT | cut -d "=" -f 2`
    SKIP_OSG=YES
    ;;
  --with-osg-framework*)
    OSG_FRAMEWORK_DIR=`echo $OPT | cut -d "=" -f 2`
    SKIP_OSG=YES
    ;;
  --with-openal-framework*)
    OPENAL_FRAMEWORK_DIR=`echo $OPT | cut -d "=" -f 2`
    ;;
  *)
    echo "Unknown option $OPT"
    exit
  esac
done

# 
#
# Configuration
# 
#

# OS version specific options
# Checking OS Major Version. 10 is Snow Leopard, 9 is Leopard, 8 is Tiger
OS_VER=`uname -r | cut -d "." -f 1`
if [ "$OS_VER" == "10" ]; then # Snow Leopard
  # gcc 4.2.1 on Snow Leopard accepts -march-core2, and it generates smaller code, I guess
  X86_CPU_OPT="-march=core2"
  SDK_PATH="$XCODE_ROOT/SDKs/MacOSX10.6.sdk"
  OSX_TARGET="10.5"
else
  if [ "$ARCH" == "x86_64" ]; then
    X86_CPU_OPT="-march=nocona"
  else
    X86_CPU_OUT="-march=prescott"
  fi
  SDK_PATH="$XCODE_ROOT/SDKs/MacOSX10.6.sdk"
  OSX_TARGET="10.5"
fi
 
# per architecture buid settings
OPT_I386="-arch i386 $X86_CPU_OPT -mfpmath=sse -msse3"
OPT_X86_64="-arch x86_64 $X86_CPU_OPT -mfpmath=sse -msse3"
OPT_PPC="-arch ppc -mtune=G4"

# Setting Architecture specific options
if [ "$ARCH" == "ppc" ]; then
  _CXXFLAGS_ARCH="$OPT_PPC $_CXXFLAGS"
elif [ "$ARCH" == "x86_64" ]; then
  _CXXFLAGS_ARCH="$OPT_X86_64 $_CXXFLAGS" 
echo "option is $_CXXFLAGS"
else
  ARCH=i386
  _CXXFLAGS_ARCH="$OPT_I386 $_CXXFLAGS" 
fi

# Destinations
FG_DIR="$PREFIX/FlightGear"
BOOST_DIR="$PWD/boost_1_48_0"
PLIB_DIR="$PREFIX/PLIB"
SG_DIR="$PREFIX/SimGear"

# Framework Paths (specify only if you want to reuse existing frameworks)
if [ ! -z "$PLIB_FRAMEWORK_DIR" ]; then
  PLIB_FRAMEWORK_DIR="$PLIB_DIR"
fi

if [ -z "$OSG_FRAMEWORK_DIR$OSG_LIB_DIR" ]; then
  OSG_FRAMEWORK_DIR="$PREFIX/OpenSceneGraph"
fi


# build settings
_CC="$GCC_PATH/gcc-4.2"
_CXX="$GCC_PATH/g++-4.2"
RELEASE_FLAGS="-O3 -DNDEBUG"
DEBUG_FLAGS="-O0 -g"

# Setting Debug / Release flags
if [ -z "$DEBUG" ]; then
  BUILD_FLAGS=$RELEASE_FLAGS
else
  BUILD_FLAGS=$DEBUG_FLAGS
fi


_CXXFLAGS="-fmessage-length=0 -pipe -fpascal-strings -fasm-blocks -fpermissive -mmacosx-version-min=$OSX_TARGET -isysroot $SDK_PATH"
_LDFLAGS="-arch $ARCH"

# Determine if you need PLIB.framework or static libs
if [ ! -z "$PLIB_FRAMEWORK_DIR" ]; then
  PLIB_OPTION="--with-plib-framework=$PLIB_FRAMEWORK_DIR"
else
  PLIB_OPTION="--with-plib=$PREFIX/PLIB"
fi

if [ ! -z "$OSG_LIB_DIR" ]; then
  OSG_OPTION="--with-osg=$OSG_LIB_DIR"
else
  OSG_OPTION="--with-osg-framework=$OSG_FRAMEWORK_DIR"
fi

if [ ! -z "$OPENAL_FRAMEWORK_DIR" ]; then
  OPENAL_OPTION="--with-openal-framework=$OPENAL_FRAMEWORK_DIR"
fi

#
#
# Building process
#
#

#
# PLIB framework
#
if [ -z "$SKIP_PLIB" ]; then
  pushd PLIB/plib
  if [ ! -f configure ]; then
    ./autogen.sh
  fi
  CXXFLAGS="$BUILD_FLAGS $_CXXFLAGS" \
  CFLAGS="$BUILD_FLAGS $_CXXFLAGS" \
  CC="$_CC" CXX="$_CXX" \
  LDFLAGS="-arch $ARCH" 
  ./configure --prefix="$PLIB_DIR/$ARCH" --enable-pw=no --enable-sl=no || exit

  make clean
  CXXFLAGS="$BUILD_FLAGS $_CXXFLAGS $_CXXFLAGS_ARCH" \
  make -e -j 8 || exit
  make install
  popd

  #
  # making PLIB.framework
  #
  mkdir -p $PLIB_DIR/$ARCH/PLIB.framework/Versions/A
  mkdir -p $PLIB_DIR/$ARCH/PLIB.framework/Headers
  mkdir -p $PLIB_DIR/$ARCH/PLIB.framework/Resources
  cp $PLIB_DIR/$ARCH/include/plib/* $PLIB_DIR/$ARCH/PLIB.framework/Headers/
  cp PLIB/info.plist $PLIB_DIR/$ARCH/PLIB.framework/Resources/
  find PLIB/plib/src/ -name "*.o" > $PLIB_DIR/$ARCH/linkfilelist

  $_CXX -arch $ARCH -dynamiclib -isysroot $SDK_PATH -LPLIB/plib/src/util -filelist $PLIB_DIR/$ARCH/linkfilelist -install_name @executable_path/../Frameworks/PLIB.framework/Versions/A/PLIB -mmacosx-version-min=10.5 -framework Carbon -framework AGL -framework Cocoa -framework GLUT -framework OpenAL -framework OpenGL -framework IOKit -single_module -compatibility_version 1.8.5 -current_version 1.8.5 -o $PLIB_DIR/$ARCH/PLIB.framework/Versions/A/PLIB

  pushd $PLIB_DIR/$ARCH/PLIB.framework
  [ -h PLIB ] || ln -s Versions/A/PLIB
  cd Versions
  [ -h Current ] || ln -s A Current
  popd
  
  echo PLIB framework for $ARCH is installed on $PLIB_DIR/$ARCH/PLIB.framework
  
  # Checking if ready for universal binary
  IS_READY4UNIV=true
  for i in $ARCHS; do [ -d "$PLIB_DIR/$i" ] || IS_READY4UNIV=false; done
  
  if [ "$IS_READY4UNIV" == "true" ]; then
    [ -d $PLIB_DIR/PLIB.framework ] && rm -rf $PLIB_DIR/PLIB.framework
    cp -R $PLIB_DIR/$ARCH/PLIB.framework $PLIB_DIR/
    if [ "$ARCHS" != "$ARCH" ]; then
      lipo -create $PLIB_DIR/*/PLIB.framework/Versions/A/PLIB -o $PLIB_DIR/PLIB.framework/Versions/A/PLIB
      echo Universal binary version of PLIB.framework is made on $PLIB_DIR
    else
      echo $ARCH version of PLIB.framework is made on $PLIB_DIR
    fi
  fi
fi


#
# OpenSceneGraph
#
 
if [ -z "$SKIP_OSG" ]; then
  OSG_VERSION=`grep -r -e "SET(OPENSCENEGRAPH" OpenSceneGraph/CMakeLists.txt | grep "_VERSION" | grep "[0-9]" | ruby -e "a=[]; readlines.each {|l| a << l.chop[-3..-2].gsub(/ /, '')}; puts a.join('.')"`
  OSG_FRAMEWORKS="OpenThreads osg osgText osgUtil osgDB osgFX osgGA osgSim osgViewer osgParticle"
  OSG_PLUGINS="osgdb_ac osgdb_bmp osgdb_dds osgdb_dxf osgdb_freetype osgdb_imageio \
               osgdb_osg osgdb_osga osgdb_rgb osgdb_tga osgdb_txf"
  OSG_PLUGIN_DIR="$PREFIX/OpenSceneGraph/osgPlugins-$OSG_VERSION"

  echo Building OpenSceneGraph ver. $OSG_VERSION...
  pushd OpenSceneGraph

  # check if OSG is capable of building frameworks and you have cmake installed
  if [ ! -z "`grep -w OSG_COMPILE_FRAMEWORKS CMakeLists.txt`" ]; then
    echo Building OSG frameworks using cmake...
    if [ ! -z `which cmake` ]; then
      #
      # building OSG using cmake
      #
      rm -rf lib
      mkdir -p lib/osgPlugins-$OSG_VERSION
      if [ "$ARCH" == "ppc" ]; then
        cmake -G "Unix Makefiles" \
          -D OSG_CONFIG_HAS_BEEN_RUN_BEFORE:BOOL=TRUE \
          -D CMAKE_CXX_COMPILER:FILEPATH=/usr/bin/g++-4.2 \
          -D CMAKE_VERBOSE_MAKEFILE:BOOL=TRUE \
          -D CMAKE_BUILD_TYPE:STRING=Release \
          -D CMAKE_CXX_FLAGS_RELEASE:STRING="$BUILD_FLAGS" \
          -D OSG_WINDOWING_SYSTEM:STRING=Cocoa \
          -D CMAKE_OSX_ARCHITECTURES:STRING=$ARCH \
          -D OSG_COMPILE_FRAMEWORKS=ON \
          -D OSG_DEFAULT_IMAGE_PLUGIN_FOR_OSX:STRING=imageio \
          -D CMAKE_CXX_FLAGS:STRING="-mtune=G4 -mcpu=G4 -ftree-vectorize -mmacosx-version-min=$OSX_TARGET" \
          -D CMAKE_OSX_SYSROOT:PATH=$SDK_PATH \
          -D OSG_COMPILE_FRAMEWORKS_INSTALL_NAME_DIR=@executable_path/../Frameworks \
          .
      else # i386 or x86_64
           # Using Cocoa in osgViewer failed to create the main window on x86_64, so using Carbon instead at this moment (2010-08-16)
           # Somehow CMAKE_OSX_ARCHITECTURES:STRING=i386 doesn't work, so need to specify -arch in CMAKE_CXX_FLAGS
        cmake -G "Unix Makefiles" \
          -D OSG_CONFIG_HAS_BEEN_RUN_BEFORE:BOOL=TRUE \
          -D CMAKE_CXX_COMPILER:FILEPATH=/usr/bin/g++-4.2 \
          -D CMAKE_VERBOSE_MAKEFILE:BOOL=TRUE \
          -D CMAKE_BUILD_TYPE:STRING=Release \
          -D CMAKE_CXX_FLAGS_RELEASE:STRING="$BUILD_FLAGS" \
          -D OSG_WINDOWING_SYSTEM:STRING=Cocoa \
          -D CMAKE_OSX_ARCHITECTURES:STRING=$ARCH \
          -D OSG_COMPILE_FRAMEWORKS=ON \
          -D OSG_DEFAULT_IMAGE_PLUGIN_FOR_OSX:STRING=imageio \
          -D CMAKE_CXX_FLAGS:STRING="-arch $ARCH $X86_CPU_OPT -mfpmath=sse -msse3 -mmacosx-version-min=$OSX_TARGET" \
          -D CMAKE_OSX_SYSROOT:PATH=$SDK_PATH \
          -D OSG_COMPILE_FRAMEWORKS_INSTALL_NAME_DIR=@executable_path/../Frameworks \
          .
      fi # [ "$ARCH" == "ppc" ]

      # build frameworks / .so needed only for fgfs
      make clean
      make $OSG_FRAMEWORKS $OSG_PLUGINS

      DEST=$PREFIX/OpenSceneGraph/$ARCH
      [ -d "$DEST" ] && rm -rf "$DEST"
      mkdir -p "$DEST"
      cp -R lib/* "$DEST/"
      OSG_FRAMEWORK_DIR="$DEST"

      # building a universal binary version of frameworks
      IS_READY4UNIV=true
      for i in $ARCHS; do [ -d "$PREFIX/OpenSceneGraph/$i" ] || IS_READY4UNIV=false; done
      if [ "$IS_READY4UNIV" == "true" ]; then
        OPENTHREADS_VER=`ls $PREFIX/OpenSceneGraph/$ARCH/OpenThreads.framework/Versions | grep -v Current`
        for i in $OSG_FRAMEWORKS; do 
          FW_VER=`ls $PREFIX/OpenSceneGraph/$ARCH/$i.framework/Versions | grep -v Current`
          [ -d $PREFIX/OpenSceneGraph/$i.framework ] && rm -rf $PREFIX/OpenSceneGraph/$i.framework
          cp -R $PREFIX/OpenSceneGraph/$ARCH/$i.framework $PREFIX/OpenSceneGraph/
          if [ "$ARCHS" != "$ARCH" ]; then
            lipo -create $PREFIX/OpenSceneGraph/*/$i.framework/Versions/Current/$i -o $PREFIX/OpenSceneGraph/$i.framework/Versions/$FW_VER/$i || exit
          fi
          # FIXME: install name is not set when you don't run make install(which builds frameworks that FG doesn't need), so need to be fixed here
          install_name_tool -id @executable_path/../Frameworks/$i.framework/Versions/$FW_VER/$i $PREFIX/OpenSceneGraph/$i.framework/Versions/$FW_VER/$i || exit

          for j in $OSG_FRAMEWORKS; do
            install_name_tool -change $PWD/lib/$j.framework/Versions/$FW_VER/$j @executable_path/../Frameworks/$j.framework/Versions/$FW_VER/$j $PREFIX/OpenSceneGraph/$i.framework/$i
          done
          install_name_tool -change $PWD/lib/OpenThreads.framework/Versions/$OPENTHREADS_VER/OpenThreads @executable_path/../Frameworks/OpenThreads.framework/Versions/$OPENTHREADS_VER/OpenThreads $PREFIX/OpenSceneGraph/$i.framework/$i

        done
        echo OpenSceneGraph frameworks are made on $PREFIX/OpenSceneGraph/
        OSG_FRAMEWORK_DIR="$PREFIX/OpenSceneGraph"

        mkdir -p $PREFIX/OpenSceneGraph/osgPlugins-$OSG_VERSION
        for i in $OSG_PLUGINS; do
          if [ "$ARCHS" != "$ARCH" ]; then
            lipo -create $PREFIX/OpenSceneGraph/*/osgPlugins-$OSG_VERSION/$i.so -o $PREFIX/OpenSceneGraph/osgPlugins-$OSG_VERSION/$i.so
          else
            cp $PREFIX/OpenSceneGraph/$ARCH/osgPlugins-$OSG_VERSION/$i.so $PREFIX/OpenSceneGraph/osgPlugins-$OSG_VERSION/
          fi
          for j in $OSG_FRAMEWORKS; do
            install_name_tool -change $PWD/lib/$j.framework/Versions/$FW_VER/$j @executable_path/../Frameworks/$j.framework/Versions/$FW_VER/$j $PREFIX/OpenSceneGraph/osgPlugins-$OSG_VERSION/$i.so
          done
          install_name_tool -change $PWD/lib/OpenThreads.framework/Versions/$OPENTHREADS_VER/OpenThreads @executable_path/../Frameworks/OpenThreads.framework/Versions/$OPENTHREADS_VER/OpenThreads $PREFIX/OpenSceneGraph/osgPlugins-$OSG_VERSION/$i.so
        done
        echo OpenSceneGraph plugins are made on $PREFIX/OpenSceneGraph/osgPlugins-$OSG_VERSION
      fi
    else
      echo "Error: OSG is capable of building frameworks, but you don't have cmake installed. Please install cmake first!"
      exit
    fi
  else
    #
    # building OSG using xcodebuild.
    #
    PROJ=Xcode/OpenSceneGraph/OpenSceneGraph.xcodeproj
    TARGET_ARCHS="ppc i386 x86_64"
    SDKPATH="$SDK_PATH"
    ARCH_OPT_i386="-msse3 $X86_CPU_OPT -mfpmath=sse"
    CCFLAGS="\
    GCC_ENABLE_SSE3_EXTENSIONS=YES \
    GCC_INLINES_ARE_PRIVATE_EXTERN=NO \
    GCC_ENABLE_SSE3_EXTENSIONS=YES \
    GCC_MODEL_TUNING=G4 \
    GCC_INLINES_ARE_PRIVATE_EXTERN=NO \
    GCC_SYMBOLS_PRIVATE_EXTERN=NO \
    MAXOSX_DEPLOYMENT_TARGET=$OSX_TARGET \
    GCC_GENERATE_DEBUGGING_SYMBOLS=$DEBUG \
    GCC_OPTIMIZATION_LEVEL=$OPT_LEVEL \
    GCC_VERSION=4.2 \
    SDKROOT=$SDKPATH \
    "
    xcodebuild -target osgFrameworks -configuration Deployment -project $PROJ ARCHS="$TARGET_ARCHS" $CCFLAGS PER_ARCH_CFLAGS_i386="$ARCH_OPT_i386" || exit
    xcodebuild -target osgPlugins -configuration Deployment -project $PROJ ARCHS="$TARGET_ARCHS" $CCFLAGS PER_ARCH_CFLAGS_i386="$ARCH_OPT_i386" || exit
  
    # installing OSG Frameworks
    # this is needed for conifgure in both SimGear and FlightGear
    [ ! -d $PREFIX/OpenSceneGraph ] && mkdir $PREFIX/OpenSceneGraph
    for i in $OSG_FRAMEWORK_DIR/*.framework; do
      [ -d  $PREFIX/OpenSceneGraph/`basename $i` ] && rm -rf $PREFIX/OpenSceneGraph/`basename $i`
      cp -R $i $PREFIX/OpenSceneGraph/
    done
    # installing plugins
    sudo mkdir -p $OSG_PLUGIN_DIR
    for i in $PWD/Xcode/OpenSceneGraph/build/Deployment/*.so; do
      sudo cp $i $OSG_PLUGIN_DIR
    done
  fi
  popd
fi

#
# SimGear using COCOA
# COCOA is needed for supporting x86_64 since Carbon provides 32bit binaries only
#
if [ -z "$SKIP_SIMGEAR" ]; then
  pushd SimGear/SimGear

  cmake -G "Unix Makefiles" -DCMAKE_OSX_ARCHITECTURES:STRING=$ARCH \
	-DCMAKE_OSX_SYSROOT:PATH="$SDK_PATH" \
	-DBoost_INCLUDE_DIR:string="$BOOST_DIR" \
	-DCMAKE_OSX_SYSROOT:PATH="$SDK_PATH" \
	-DCMAKE_OSX_DEPLOYMENT_TARGET:STRING=$OSX_TARGET \
	-DCMAKE_CXX_FLAGS_RELEASE:STRING="$_CXXFLAGS_ARCH -fasm-blockss" \

  make clean
  make || exit
  sudo make install
  popd

  # Checking if ready for universal binary
#  IS_READY4UNIV=true
#  for i in $ARCHS; do [ -d "$SG_DIR/$i" ] || IS_READY4UNIV=false; done
#
#  if [ "$IS_READY4UNIV" == "true" ]; then
#    mkdir -p $SG_DIR/lib
#    mkdir -p $SG_DIR/include
#    if [ "$ARCHS" != "$ARCH" ]; then
#      for i in $SG_DIR/$ARCH/lib/*.a; do
#        lipo -create $SG_DIR/*/lib/`basename $i` -o $SG_DIR/lib/`basename $i`
#      done 
#    else
#      cp -R $SG_DIR/$ARCH/lib/* $SG_DIR/lib/
#    fi
#    cp -R $SG_DIR/$ARCH/include/simgear $SG_DIR/include/
#    echo SimGear Libs are made on $PLIB_DIR
#  fi
fi

#
# FlightGear
#
if [ -z "$SKIP_FG" ]; then
  pushd flightgear/FlightGear
#  if [ ! -f configure ]; then
#    ./autogen.sh
#  fi
#  CXXFLAGS="-I$BOOST_DIR -Wall -DENABLE_AUDIO_SUPPORT -DHAVE_CONFIG_H $BUILD_FLAGS $_CXXFLAGS" \
#  CFLAGS="$CXXFLAGS" \
#  CC="$_CC -arch $ARCH" CXX="$_CXX -arch $ARCH" LDFLAGS="$_LDFLAGS"  \
#  ./configure --prefix="$FG_DIR/$ARCH" $PLIB_OPTION $OSG_OPTION --with-simgear="$SG_DIR" $OPENAL_OPTION \
#  --with-boost="$BOOST_DIR" --with-boost-libdir="$BOOST_DIR" --enable-osgviewer --with-eventinput \
#  --with-cocoa-framework \
#  --with-alut-framework=/Library/Frameworks || exit


  # FG's configure enables JPEG SERVER even your Mac doesn't have libjpeg, so force disable it.
  # (This is because Mac's gcc accepts (or ignores?) -ljpeg even you don't have libjpeg.a, and thus FG's configure believe there exists libjpeg)
  # You can cut the next line if you know your Mac has libjpeg
##  cp src/Include/config.h /tmp; cat /tmp/config.h | sed 's!#define FG_JPEG_SERVER 1!/* #under FG_JPEG_SERVER */!' > src/Include/config.h

cmake -G "Unix Makefiles" -DCMAKE_OSX_ARCHITECTURES:STRING=i386 \
	-DCMAKE_OSX_SYSROOT:PATH="SDK_PATH" \
	-DBoost_DIR:string="$BOOST_DIR" \
	-DCMAKE_OSX_SYSROOT:PATH="$SDK_PATH" \
	-DCMAKE_OSX_DEPLOYMENT_TARGET:STRING=10.5 \
	-DCMAKE_CXX_FLAGS_RELEASE:STRING="-O3 -DNODEBUG -mfpmath=sse -msse3 -march=prescott" \
	-DSIMGEAR_INCLUDE_DIR:PATH="/usr/local/include" \
	-DSIMGEAR_VERSION_OK:STRING="$TARGET_VERSION" \
	-DCMAKE_INSTALL_PREFIX:PATH="$PWD/build/FlightGear"

#  make clean
  make || exit

  make install || exit
  popd

  # building universal binaries
  # Checking if ready for universal binary
#  IS_READY4UNIV=true
#  for i in $ARCHS; do [ -d "$FG_DIR/$i" ] || IS_READY4UNIV=false; done
#
#  if [ "$IS_READY4UNIV" == "true" ]; then
#    mkdir -p $FG_DIR/bin
#    if [ "$ARCHS" != "$ARCH" ]; then
#      for i in $FG_DIR/$ARCH/bin/*; do
#        lipo -create $FG_DIR/*/bin/`basename $i` -o $FG_DIR/bin/`basename $i`
#      done 
#    else
#      cp -R $FG_DIR/$ARCH/bin/* $FG_DIR/bin/
#    fi
#    echo FlightGear binaries are made on $FG_DIR/bin

    # ALUT's install_name is sometimes wrong ($HOME/Library/Frameworks/ALUT.framework/Versions/A/ALUT, but should begin with @executable_path)
#    install_name_tool -change $HOME/Library/Frameworks/ALUT.framework/Versions/A/ALUT @executable_path/../Frameworks/ALUT.framework/Versions/A/ALUT $FG_DIR/bin/fgfs
#  fi
fi

echo Done. 
