# set -ex

PREFIX=$PWD/build
BUILD_ROOT=$PWD
if [ ! -d $PREFIX ]; then
  mkdir -p $PREFIX
fi

OSX_MINOR_VERSION=`uname -r | cut -d '.' -f 1`
if [ "$OSX_MINOR_VERSION" = "9" ]; then
  echo "***********************************************************"
  echo "You need to have Mac OS X 10.4.x for building svn libraries"
  echo "that are compatible with OS X 10.4. "
  echo "Building svn libraries using this script will make libraries"
  echo "that ONLY work on Mac OS X 10.5 or your current OS X version"
  echo ""
  echo "Building svn libraries cancelled."
  echo "***********************************************************"
  exit
fi

SDKROOT=/Developer/SDKs/MacOSX10.4u.sdk
_CCFLAGS="-isysroot $SDKROOT -mmacosx-version-min=10.4 -arch ppc -arch i386 -O2"
_LDFLAGS="-isysroot $SDKROOT -mmacosx-version-min=10.4 -arch ppc -arch i386"

echo "Let me build svn libraries for terrasync - the On-the-fly scenery downloader."

echo "Building expat"
pushd expat-2.0.1
LDFLAGS=$_LDFLAGS CFLAGS=$_CCFLAGS ./configure --prefix=$PREFIX --enable-static --disable-dynamic --disable-debug
make clean
DESTDIR=$PREFIX make -j 4
make install
popd

echo "Building apr"
pushd apr-1.3.3
CFLAGS=$_CCFLAGS LDFLAGS="$_LDFLAGS -L$PREFIX/lib" ./configure --prefix=$PREFIX --enable-static --disable-dynamic --disable-debug
make clean
DESTDIR=$PREFIX make -j 4
make install
popd

echo "Building apr-util"
pushd apr-util-1.3.4
SDK=/Developer/SDKs/MacOSX10.5.sdk
CFLAGS=$_CCFLAGS LDFLAGS="$_LDFLAGS -L$PREFIX/lib" ./configure --prefix=$PREFIX --with-apr=$PREFIX --enable-static --disable-dynamic --disable-debug --with-expat=$PREFIX
make clean
DESTDIR=$PREFIX make -j 4
make install
popd

echo "Building neon"
pushd neon-0.28.3
CFLAGS=$_CCFLAGS LDFLAGS="$_LD_FLAGS -L$PREFIX/lib" ./configure --prefix=$PREFIX --enable-static --disable-dynamic --disable-debug --with-ssl=openssl --with-pakchois
make clean
DESTDIR=$PREFIX make -j 4
make install
popd

echo "Building svn libraries"
SDK=/Developer/SDKs/MacOSX10.5.sdk
pushd subversion-1.5.5
CFLAGS=$_CCFLAGS ./configure --prefix=$PREFIX --enable-static --disable-dynamic --disable-debug --with-apr=$PREFIX --with-apr-util=$PREFIX --with-neon=$PREFIX --with-expat=$PREFIX --with-xml2=/usr --with-ssl=/usr --without-swig --without-swig-py --without-python
make clean
DEST_DIR=$PREFIX \
EXTRA_CFLAGS="-arch ppc -arch i386" EXTRA_LDFLAGS="-L$PREFIX/lib" \
make -j 4 lib install-lib install-include install-neon-lib
popd
find ./build -name "*.dylib" -exec rm -rf {} \;

echo "Done"
