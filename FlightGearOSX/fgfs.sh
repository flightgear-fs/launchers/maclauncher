#!/bin/sh

# fgfs.sh
# FlightGearOSX
#
# Created by Tatsuhiro Nishioka on 12/4/06.
# Copyright 2006 Tat. All rights reserved.

LOG_DIR="$HOME/Library/Application Support/FlightGear"
[ ! -d "$LOG_DIR" ] && mkdir -p "$LOG_DIR"

DYLD_LIBRARY_PATH="$PWD/../Frameworks"  exec ../MacOS/fgfs --fg-root="$PWD/../Resources/data" $* > "$LOG_DIR/log.txt" 2>&1
