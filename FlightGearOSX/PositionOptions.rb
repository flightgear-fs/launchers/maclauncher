#
#  PositionOptions.rb
#  FlightGearOSX
#
#  Created by Tatsuhiro Nishioka on 12/18/06.
#  Copyright (c) 2006 Tatsuhiro Nishioka
#  http://macflightgear.sourceforge.net/
# 
#  This file is part of FlightGearMacOSX.
#
#  FlightGearMacOSX is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  FlightGearMacOSX is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with FlightGearMacOSX. If not, see <http://www.gnu.org/licenses/>
#


require 'osx/cocoa'
require 'AirportDictionary'
require 'SearchableOptions'
require 'ParkingPositions'

include OSX
#
# UI Controller for Airport/Runway Options
#
class RunwayOption < SelectableOption
  def initialize(name, outlet, items=[NSLocalizedString("default")], default=nil)
    @defaultRunway=NSLocalizedString("default")
    @code = Preferences.valueForKey('airport').to_s
    @@inst = self
    begin
      runways = [@defaultRunway] + 
        AirportDictionary.instance.searchForCode(@code).first.runways
       default = @defaultRunway
    rescue
      puts $!.message
      runways=[@defaultRunway]
      default=@defaultRunway
    end
    super(name, outlet, runways, default)
  end
  
  def generate()
    if (@outlet.indexOfSelectedItem() == 0 || @value.downcase == @defaultRunway)
      save()
      return ''
    else
      save()
      return super()
    end
  end

  def save()
    @value = 'default' if (@value == @defaultRunway)
    super()
  end

  def _setAirport(airport)
    begin
      if (airport.code.to_s != @code && @outlet)
        @code = airport.code.to_s
        @outlet.removeAllItems
        @outlet.addItemWithTitle(@defaultRunway)
        airport.runways.each {|runway| @outlet.addItemWithTitle(runway)}
        @outlet.selectItemWithTitle(@defaultRunway)
      end		
    rescue
      puts $!.message
      puts $!.backtrace.join("\n")
    end
  end

  def self.setAirport(airport)
    @@inst._setAirport(airport) if (defined?(@@inst))
  end

  def self.instance()
    return @@inst if (defined?(@@inst))
    return nil
  end
end

class ParkingOption < SelectableOption
  def initialize(name, outlet, items=[NSLocalizedString("default")], default=nil)
    @defaultParkPos = NSLocalizedString("default")
    positions = [@defaultParkPos]
    @code = Preferences.valueForKey('airport').to_s
    list = $parkingPositions[@code]
    default = @defaultParkPos # if (default == 'default')
    positions += list.uniq.sort if (list)
    @@inst = self
    super(name, outlet, positions, default)
  end

  def generate()
    save()
    if (@outlet.indexOfSelectedItem() == 0 || (@value && @value.downcase == @defaultParkPos))
      return '' 
    else
      return super()
    end
  end

  def save()
    @value = 'default' if (@value == @defaultParkPos)
    super()
  end

  def _setAirport(airport)
    if (airport.code.to_s != @code && @outlet)
      @code = airport.code.to_s
      @outlet.removeAllItems
      @outlet.addItemWithTitle(@defaultParkPos)
      positions = $parkingPositions[@code]
      if (positions)
        positions.sort.each { |position| @outlet.addItemWithTitle(position) }
      end        
      @outlet.selectItemWithTitle(@defaultParkPos)
    end
  end

  def self.setAirport(airport)
    @@inst._setAirport(airport) if (defined?(@@inst))
  end

  def self.instance()
    return @@inst if (defined?(@@inst))
    return nil
  end
end  
  
        
#
# UI Controller
#
class PositionOptions < SearchableOptions
  include OptionHolder

  ib_outlets :runway, :parkpos

  def awakeFromNib()
    @dictionary = AirportDictionary.instance
    super()
  end

  def synchronize(notification=nil)
    if (!defined?(@parser))
      bindings = {:runway => RunwayOption, :parkpos => ParkingOption}
      @parser = OptionParser.new(self, bindings)
    end
    filter = @entry.stringValue().to_s.split(':').first
    filter = Preferences.valueForKey('airport') || 'KSFO' if (!filter)
    @searchBox.setStringValue(filter)
    search {|code| @dictionary.searchForCode(code) } if (@entry)
    return super()
  end

  # TableView delegations
  def setEntry(sender=nil)
    if (super())
      RunwayOption.setAirport(@selected)
      ParkingOption.setAirport(@selected)
    end
  end

  # Actions for PopupButtons
  def runwayChanged()
    # parking position needs to be 'default' when a runway is selected
    ParkingOption.instance.reset()
  end
  
  def parkingChanged()
    # runway needs to be 'default' when a parking position is selected
    RunwayOption.instance.reset()
  end
  
  alias tableViewSelectionDidChange setEntry
end
