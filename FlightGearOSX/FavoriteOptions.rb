#
#  FavoriteOptions.rb
#  FlightGearOSX
#
#  Created by Tatsuhiro Nishioka on 1/8/08.
#  Copyright (c) 2008 Tatsuhiro Nishioka
#  http://macflightgear.sourceforge.net/
# 
#  This file is part of FlightGearMacOSX.
#
#  FlightGearMacOSX is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  FlightGearMacOSX is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with FlightGearMacOSX. If not, see <http://www.gnu.org/licenses/>
#

require 'osx/cocoa'
require 'SearchableOptions'
require 'FavoriteDictionary'

include OSX

#
# UI Controller for Favorite Options
#
class FavoriteOptions < SearchableOptions
  ib_outlets :favoriteNamePanel, :favoriteName

  def awakeFromNib()
    @dictionary = FavoriteDictionary.instance
    super()
  end

  def setEntry(sender=nil)
  end
  
  def openFavoriteNamePanel(sender = nil)
    NSApp.beginSheet_modalForWindow_modalDelegate_didEndSelector_contextInfo(@favoriteNamePanel, OSX::NSApp.mainWindow, nil, nil, nil)
  end

  def closeFavoriteNamePanel(sender = nil)
    @favoriteNamePanel.orderOut(nil)
    NSApp.endSheet(@favoriteNamePanel)
  end

  def addFavorite(sender = nil)
    @dictionary.add(@favoriteName.stringValue())
    synchronize(nil)
    closeFavoriteNamePanel()
  end
  
  def removeFavorite(sender = nil)
    @dictionary.remove(@table.selectedRow.to_i) if (@table.selectedRow >= 0)
    @table.reloadData()
  end
  
  def search(sender = nil)
    @dictionary.search("")
    @table.reloadData()
  end

  def synchronize(sender = nil)
    # do nothing when synchronize is called from apply()
    return if (sender == self)
    search()
    return super()
  end

  def apply(arg=nil)
    begin
      @dictionary.apply(@table.selectedRow.to_i) if (@table.selectedRow >= 0)
      # AircraftOptions and AirportOptions must be synchronized
      # since it needs to update searchbox and tableview.
      ObjectSpace.each_object(SearchableOptions) do |position|
        position.synchronize(self)
      end
      NSNotificationCenter.defaultCenter.postNotificationName_object("FGAircraftDidSelectNotification", self)
    rescue RuntimeError
      puts $!
    end
  end

  def optionsWillTerminate(notification=nil)
    @dictionary.save()
    super()
  end

  alias tableViewSelectionDidChange setEntry

  def copyWithZone()
  end
end
