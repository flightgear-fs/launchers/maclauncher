#
#  FlightGearWindow.rb
#  FlightGearOSX
#
#  Created by Tatsuhiro Nishioka on 12/1/06.
#  Copyright (c) 2006 Tatsuhiro Nishioka.
#  http://macflightgear.sourceforge.net/
# 
#  This file is part of FlightGearMacOSX.
#
#  FlightGearMacOSX is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  FlightGearMacOSX is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with FlightGearMacOSX. If not, see <http://www.gnu.org/licenses/>
#


require 'osx/cocoa'

include OSX

#
# This class shrinks the height of the window when launched
# I made this since I don't want to shrink the window size 
# everytime I change advanced options on InterfaceBuilder.
# 
class FlightGearWindow < NSWindow
  @@shrunk = false

  def setFrame_display(frame, disp)
    theFrame = frame

    if (!@@shrunk)
      # shrink the frame when it is called for the first time.
      ObjectSpace.each_object(NSTabView) {|tab| @tab = tab}
      if (defined?(@tab))
        delta = @tab.frame.size.height + 8
      else
        # just in the case it fails to get an NSTabView object
        # FIXME: This value shoud be obtained through NIB file..
        delta = 320
      end
      theFrame.size.height -= delta
      theFrame.origin.y += delta
      @@shrunk = true
      disp = false
    else
      disp = true
    end
    
    super_setFrame_display(theFrame, disp)
#      setBackgroundColor(NSColor.colorWithPatternImage(NSImage.imageNamed("panel.png")))
  end
end
