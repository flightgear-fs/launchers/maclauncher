#
#  RenderingOptions.rb
#  FlightGearOSX
#
#  Created by Tatsuhiro Nishioka on 11/28/06.
#  Copyright (c) 2006 Tatsuhiro Nishioka
#  http://macflightgear.sourceforge.net/
# 
#  This file is part of FlightGearMacOSX.
#
#  FlightGearMacOSX is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  FlightGearMacOSX is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with FlightGearMacOSX. If not, see <http://www.gnu.org/licenses/>
#

require 'osx/cocoa'
require 'singleton'
require 'OptionParser'
#
# Controller for Rendering options
#
class RenderingOptions < NSObject
  include OptionHolder
  ib_outlets :fog, :fullScreen, :shading, :geometry, :antiAliasing, :textures, :wireFrame

  def awakeFromNib()
    @bindings = {:fog => [SwitchableOption, ['disable', 'fastest', 'nicest'], 'nicest'], 
      :fullScreen => [BoolOption, 'fullscreen', false],
      :shading => [SwitchableOption, ['smooth', 'flat'], 'smooth'],
      :antiAliasing => [AntiAliasingOption, [0, 8], 0],
      :geometry => [SelectableOption, ['640x480', '800x600', '1024x768', '1280x700', '1280x800', '1280x1024', '1366x768', '1344x756', '1440x900', '1680x1050', '1920x1200', '2560x1440', '2560x1600'], '800x600'], 
      :textures => [BoolOption, nil, true],
      :wireFrame => [BoolOption, nil, false]
    }

    @parser = OptionParser.new(self, @bindings)
  end
end
