#
#  KeyBinds.rb
#  FlightGearOSX
#
#  Created by Tatsuhiro Nishioka on 11/28/06.
#  Copyright (c) 2006 Tat. All rights reserved.
#
#
# This module is not implemented yet, do not touch!
#

require 'rexml/document'

class KeyBind
	def initialize(element, base_name = nil)
		@element = element
		if (base_name)
			@name = base_name
			@modifier = 'shift'
		else
			@name = element.get_text('name')
		end
		@orgName = @name
		@desc = element.get_text('desc')
		@binding = element.elements['binding']
		@repeatable = element.elements['repeatable']
		@modUp = element.elements['mod-up']
	end

	def definition()
		return "#{@modifier}#{@name}=#{@desc}"
	end

	def to_s()
		buf=''
		buf += "  <name>#{@name}</name>\n"
		buf += "  <mod-shift>\n  " if (defined?(@modifier))
		buf += "  <desc>#{@desc}</desc>\n"
		buf += "  #{@repeatable}\n" if (@repeatable)
		buf += "  #{@binding}\n"
		buf += "  #{@modUp}\n" if (@modUp)
		buf += "  </mod-shift>\n" if (defined?(@modifier))
		return buf
	end

	def assign(key)
		@name = key
		@dirty = true
	end
	
	attr_reader :name, :desc, :binding, :mod_up, :modifier
end

class KeyBindings
	include REXML
	def initialize(file)
		begin
			@bindings = []
			@document = Document.new(File.new(file))
			@document.elements.each('PropertyList/*') do |element| 
				@bindings << KeyBind.new(element)
				shift = element.elements['mod-shift']
				@bindings << KeyBind.new(shift, element.get_text('name')) if (shift)
			end
		rescue
			puts 'error'
			puts $!
		end
	end

	attr_reader :bindings
end

=begin
keyBind = KeyBinding.new('/Applications/FlightGear.app/Contents/Resources/data/keyboard.xml')
keyBind.bindings.each {|elem|
	puts elem.definition() + "\n"
}

=end
