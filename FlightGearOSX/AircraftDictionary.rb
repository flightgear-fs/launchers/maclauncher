#
#  AircraftDictionary.rb
#  FlightGearMacOSX
#
#  Created by Tatsuhiro Nishioka on 01/09/08.
#  Copyright (c) 2008 Tatsuhiro Nishioka
#  http://macflightgear.sourceforge.net/
# 
#  This file is part of FlightGearMacOSX.
#
#  FlightGearMacOSX is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  FlightGearMacOSX is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with FlightGearMacOSX. If not, see <http://www.gnu.org/licenses/>
#

require 'rexml/document'
require 'osx/cocoa'
require 'Dictionary'
require 'singleton'
include OSX

def getResourcePath
  if (File.basename($0) == __FILE__)
    return $resourcePath
  else
    return NSBundle.mainBundle.resourcePath
  end
end

#
# This class speeds up loading Aircraft Entries using Cache
# up to 10 times.
# 
class AircraftEntryCache
  include Singleton

  def initialize()
    dir=getResourcePath()
    @cacheFile = dir + '/data/AircraftCache.rb'
    begin
      require @cacheFile
      $cache.default = nil
    rescue LoadError
      puts "Can't load aircraft cache file, ignoring..."
      $cache = {}
    end
  end

  def mergeXMLDocuments(doc1, doc2)
    doc2.elements.each('PropertyList/*') do |element|
      doc1.elements['PropertyList'].add_element(element)
    end
  end

  def readXMLFile(file)
    info = nil
    begin
      info = REXML::Document.new(File.new(file))   
      includedFile = info.elements['PropertyList'].attributes['include']
      if (includedFile)
        includedDocument = readXMLFile(File.dirname(file) + '/' + includedFile)
        mergeXMLDocuments(info, includedDocument)
      end
    rescue
      puts "Error while reading #{file}. Skip loading..."
      puts $!.to_s
    end
    return info
  end

  def loadFromFile(file)
    @file = file
    @code = File.basename(@file).gsub(/-set\.xml/, '');
    info = readXMLFile(file)
    begin
      @name = info.elements['PropertyList/sim/description'].text
      @fdm = info.elements['PropertyList/sim/flight-model'].text
      if (info.elements['PropertyList/sim/status'])
        @status = info.elements['PropertyList/sim/status'].text
      else
        @status = 'unknown'
      end
    rescue
      @name = nil
      $cache[@file]='skip'
      puts "#{File.basename(file)} is not a valid aircraft file. skipping..."
      return nil
    end
    @imageFile = File.dirname(@file) + '/thumbnail.jpg'
    if (!FileTest.exists?(@imageFile) && File.fnmatch?("*.jpg", File.dirname(@file)))
      @imageFile = File.dirname(@file) + `ls #{File.dirname(@file)}/*.jpg`.chomp.split(/\n/).first
    end
    updateCache(file)
    return [@name, @fdm, @status, @imageFile]
  end
  
  def updateCache(file)
    $cache[file]={}
    $cache[file][@stat.mtime.to_s+':'+@stat.size.to_s]=[@name, @fdm, @status, @imageFile]
  end
  
  def getFromCache(file)
    cacheInfo = nil
    @stat = File.stat(file)
    if ($cache && $cache.size > 0)
      if ($cache[file].kind_of?(String))
#        puts "#{File.basename(file)} is skipped"
        return nil
      end
      cachedInfo = $cache[file][@stat.mtime.to_s+':'+@stat.size.to_s]  if ($cache[file])
      return cachedInfo if (cachedInfo)
    end
#    puts "#{File.basename(file)} is updated. reload from xml file"
  end

  def saveCache()
    $cache.delete_if {|key, value| !File.exist?(key) }
    begin
      buffer = File.new(@cacheFile, "w")
      buffer.write("$cache=")
      buffer.puts($cache.inspect.split("},").join("},\n"))
      buffer.close
    rescue Errno::ENOENT
      puts "Can't open aircraft cache data file: #{@cacheFile}"
    rescue Errno::EROFS, Errno::EACCES
      NSRunAlertPanel(NSLocalizedString("Warning").to_s, 
                      NSLocalizedString("Read Only File System Warning").to_s,
                      nil,nil,nil)
    end
  end

  def load(file)
    info = getFromCache(file)
    if (!info)
      info = loadFromFile(file)
    end
    return info      
  end
end

#
# Aircraft Entries
#
class AircraftEntry < NSObject
  def initWithFile(file)
    data = AircraftEntryCache.instance.load(file)
    return nil if (!data)
    @name, @fdm, @status, @imageFile = data
    @code = File.basename(file).gsub(/-set\.xml/, '')
    @path=file
    return self
  end

  def getSearchables()
    return [@code.to_s, @name.to_s, @status.to_s, @fdm.to_s]
  end

  def getImage()
    return @imageFile
  end

  def stringValue()
    return @name
  end

  attr_reader :name, :status, :fdm, :imageFile, :code, :path
end

#
# Aircraft Entry Dictionary - for TableView
#
class AircraftDictionary < Dictionary
  def self.instance()
    if (!defined?(@@inst))
      @@inst = AircraftDictionary.alloc.initWithPath(getResourcePath() + '/data/Aircraft')
    end
    return @@inst
  end

  def initWithPath(path)
    @path = path
    reload()
    return self
  end

  def load()
    super()
    files = Dir.glob(@path + "/*/*-set.xml")
    for i in (0..files.size-1) do
      aircraft = AircraftEntry.alloc.initWithFile(files[i])
      @entries.push(aircraft) if (aircraft && aircraft.description)
    end
    AircraftEntryCache.instance.saveCache()
  end
  
  alias reload load  
end

# Unit test
if (File.basename($0) == __FILE__)
  $resourcePath = "./"
  dict = AircraftDictionary.instance
  dict.load()
end
