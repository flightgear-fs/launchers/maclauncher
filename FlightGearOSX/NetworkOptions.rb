#
#  NetworkOptions.rb
#  FlightGearOSX
#
#  Created by Tatsuhiro Nishioka on 11/28/06.
#  Copyright (c) 2006 Tatsuhiro Nishioka.
#  http://macflightgear.sourceforge.net/
# 
#  This file is part of FlightGearMacOSX.
#
#  FlightGearMacOSX is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  FlightGearMacOSX is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with FlightGearMacOSX. If not, see <http://www.gnu.org/licenses/>
#


require 'osx/cocoa'
require 'OptionParser'
require 'Options'

class NetworkOptions < OSX::NSObject
  include OptionHolder
    ib_outlets :callsign, :server, :serverPort, :isMultiplay, :fgcomServer, :fgcomMode

  def awakeFromNib()
    bindings = {:callsign => CallSign, :server => Server, :isMultiplay => MultiPlay,
                :fgcomServer => FGComServer,
                :fgcomMode => FGComMode
    }
    @parser = OptionParser.new(self, bindings)
  end
  
#  def getFGCOMAccount(sender = nil)
#    `open http://macflightgear.sourceforge.net/home/documents/users-guide#how-to-get-fgcom-account`
#  end
end
