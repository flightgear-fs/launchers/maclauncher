#
#  FeaturesOptions.rb
#  FlightGearOSX
#
#  Created by Tatsuhiro Nishioka on 11/28/06.
#  Copyright (c) 2006 Tatsuhiro Nishioka.
#  http://macflightgear.sourceforge.net/
# 
#  This file is part of FlightGearMacOSX.
#
#  FlightGearMacOSX is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  FlightGearMacOSX is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with FlightGearMacOSX. If not, see <http://www.gnu.org/licenses/>
#

require 'osx/cocoa'
require 'OptionParser'

#
# Controller for Variable Bool/Value options
#
class FeaturesOptions < OSX::NSObject
  include OptionHolder
  ib_outlets :clockFreeze, :culled, :freeze, :fuelFreeze, :hud, :hud3d, :panel, :randomObjects, :sound, :tres, 
  :realWeatherFetch, :horizonEffect, :clouds, :clouds3d, :turbulence, :visibility, :aiModels
  def awakeFromNib()
    @bindings = {:clockFreeze => [BoolOption, 'clock-freeze', true],
      :freeze => [BoolOption, nil, false],
      :fuelFreeze => [BoolOption, 'fuel-freeze', false],
      :hud => [BoolOption, nil, true],
      :hud3d => [BoolOption, 'hud-3d', false],
      :panel => [BoolOption, nil, true],
      :randomObjects => [BoolOption, 'random-objects', true],
      :aiModels => [BoolOption, 'ai-models', true],
      :sound => [BoolOption, nil, true],
      :realWeatherFetch => [BoolOption, 'real-weather-fetch', true],
      :horizonEffect => [BoolOption, 'horizon-effect', true],
      :clouds => [BoolOption, 'clouds', true],
      :clouds3d => [BoolOption, 'clouds3d', true],
      :turbulence => [ValueOption, '0.1'],
      :visibility => [ValueOption, '16093']
    }
    @parser = OptionParser.new(self, @bindings)
  end
end
