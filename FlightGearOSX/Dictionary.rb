#
#  Dictionary.rb
#  FlightGearOSX
#
#  Created by Tatsuhiro Nishioka on 01/09/08.
#  Copyright (c) 2008 Tatsuhiro Nishioka
#  http://macflightgear.sourceforge.net/
# 
#  This file is part of FlightGearMacOSX.
#
#  FlightGearMacOSX is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  FlightGearMacOSX is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with FlightGearMacOSX. If not, see <http://www.gnu.org/licenses/>
#

require 'osx/cocoa'

#
# Base class of Aircraft/Airport Dictionary that is a
# collection of Searchable objects using TableView
#
# Use of 'each' is not recommended since it is very slow.
# Reading/searching bunch of airport/aircraft data is time 
# consuming, so using 'for-loop' is way better
# 
class Dictionary < OSX::NSObject  
  def init()
    return self
  end
  
  def load()
    @entries = []
    @searchResult = []
  end

  def checkNullSearch(keyword)
    @searchResult = []
    if (keyword == '')
      @searchResult = @entries
      return true
    end
    return false
  end

  def search(keyword)
    return @searchResult if (checkNullSearch(keyword))
    key = keyword.downcase
    for i in (0..@entries.size() -1) do
      list = @entries[i].getSearchables().join(";").downcase
      @searchResult << @entries[i] if (list[key])
    end
    return @searchResult
  end

  def searchForCode(code)
    return @searchResult if (checkNullSearch(code))
    for i in (0..@entries.size-1) do
      @searchResult << @entries[i] if (@entries[i].code.to_s == code)
    end
    return @searchResult
  end

  def searchForName(name)
    return @searchResult if (checkNullSearch(name))
    for i in (0..@entries.size-1) do
      @searchResult << @entries[i] if (@entries[i].name.to_s == name)
    end
    return @searchResult
  end

  def valueForName(name)
    for i in (0..@entries.size-1) do
      return @entries[i] if (@entries[i].name().to_s == name)
    end
    return nil
  end    

  def valueForCode(code)
    for i in (0..@entries.size-1) do
      return @entries[i] if (@entries[i].code().to_s == code)
    end
    return nil
  end

  # implementation for TableView DataSource
  def tableView_objectValueForTableColumn_row(tableView, column, row)
    return @searchResult[row].valueForKey(column.identifier)
  end
  
  def numberOfRowsInTableView(tableView)
    load() if (!defined?(@entries))
    return @searchResult.size
  end
  
  attr_reader :searchResult
end
