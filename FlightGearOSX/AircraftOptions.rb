#
#  AircraftOptions.rb
#  FlightGearOSX
#
#  Created by Tatsuhiro Nishioka on 1/8/08.
#  Copyright (c) 2008 Tatsuhiro Nishioka
#  http://macflightgear.sourceforge.net/
# 
#  This file is part of FlightGearMacOSX.
#
#  FlightGearMacOSX is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  FlightGearMacOSX is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with FlightGearMacOSX. If not, see <http://www.gnu.org/licenses/>
#

require 'osx/cocoa'
require 'SearchableOptions'

include OSX

#
# UI Controller for Aircraft Options
#
class AircraftOptions < SearchableOptions

  def awakeFromNib()
    @dictionary = AircraftDictionary.instance
    super()
  end
  
  def setEntry(sender=nil)
    if (super())
      NSNotificationCenter.defaultCenter.postNotificationName_object("FGAircraftDidSelectNotification", self)
    end
  end

  def synchronize(sender = nil)
    @searchBox.setStringValue(@entry.stringValue())
    search {|name| @dictionary.searchForName(name)}
    return super()
  end

  def openAircraftLink(sender = nil)
    `open http://macflightgear.sourceforge.net/home/documents/aircraft-links/`
  end

  def preview(sender = nil)
    path = NSBundle.mainBundle.resourcePath.to_s
    configFile = @selected.path()
    modelFile = AircraftEntryCache.instance.readXMLFile(configFile).elements['PropertyList/sim/model/path'].text
    if (modelFile =~ /.*\.xml/) 
      @previewThread = Launcher.new(path, 'fgviewer.sh', [modelFile])
      @previewThread.join()
    end
  end

  alias tableViewSelectionDidChange setEntry
end
