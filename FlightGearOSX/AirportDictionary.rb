#
#  AirportDictionary.rb
#  FlightGearOSX
#
#  Created by Tatsuhiro Nishioka on 12/18/06.
#  Copyright (c) 2006 Tatsuhiro Nishioka
#  http://macflightgear.sourceforge.net/
# 
#  This file is part of FlightGearMacOSX.
#
#  FlightGearMacOSX is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  FlightGearMacOSX is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with FlightGearMacOSX. If not, see <http://www.gnu.org/licenses/>
#

require 'osx/cocoa'
require 'singleton'
require 'AirportData'

include OSX

#
# Airport Entry
#
class AirportEntry < NSObject
  def initWithDictionary(dict)
    @code = dict['code']
    @shortName = dict['short name']
    @longName = dict['long name']
    @city = dict['city']
    @country = dict['country']
    @iata = dict['iata']
    @runways = dict['runways']
    @lon = dict['lon']
    @lat = dict['lat']
    @runways += getAdditionalRunways(@runways)
    @runways.sort!.reverse!
    return self
  end

  def name()
    return @longName if (@longName.length > 0 && @longName.length > @shortName.length)
    return @shortName
  end

  def getAdditionalRunways(runways)
    additional = []
    for i in (0..runways.size-1) do
      id = runways[i]
      if (id =~ /[A-Za-z]$/)
        postfix = id[-1,1]
        direction = id[0,2].to_i
      else
        postfix = ""
        direction = id.to_i
      end
      additional.push((direction > 18 ? direction - 18 : direction + 18).to_s + postfix)
    end
    return additional
  end

  def getLocation()
    return {'lon' => @lon, 'lat' => @lat }
  end

  def getSearchables()
    return [@code, @shortName, @longName, @city, @country, @iata]
  end

  def stringValue()
    return @code + ": " + name()
  end

  attr_reader :code, :shortName, :iata, :longName, :city, :country, :runways
end

#
# Data source for the TableView
#
class AirportDictionary < Dictionary
  def self.instance()
    if (!defined?(@@inst))
      @@inst = AirportDictionary.alloc.init()
      @@inst.load()
    end
    return @@inst
  end

  def load()
    super()
    for i in (0..$airportData.size() -1) do
      @entries << AirportEntry.alloc.initWithDictionary($airportData[i])
    end
  end  
end

