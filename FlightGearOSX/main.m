//
//  main.m
//  FlightGearMacOSX
//
//  Created by Tatsuhiro Nishioka on 11/14/06.
//  Copyright (c) 2006 Tat. All rights reserved.
//

#import <RubyCocoa/RBRuntime.h>
#import <AppKit/AppKit.h>

@interface LauncherThread : NSObject {
}
+ (void)entry:(id)param;
@end

@implementation LauncherThread
+ (void)entry:(id)param;
{
  [NSThread exit];
}
@end

int main(int argc, const char *argv[])
{	
	[NSThread detachNewThreadSelector:@selector(entry:) toTarget:[LauncherThread class] withObject:nil];
    return RBApplicationMain("rb_main.rb", argc, argv);
}
