#
#  Options.rb
#  FlightGearMacOSX
#
#  Created by Tatsuhiro Nishioka on 11/23/06.
#  Copyright (c) 2006 Tatsuhiro Nishioka
#  http://macflightgear.sourceforge.net/
# 
#  This file is part of FlightGearMacOSX.
#
#  FlightGearMacOSX is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  FlightGearMacOSX is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with FlightGearMacOSX. If not, see <http://www.gnu.org/licenses/>
#

require 'osx/cocoa'
require 'socket'
require 'OptionParser'
require 'Preferences'
require 'AirportDictionary'
require 'AircraftDictionary'

include OSX

# Concrete options that cannot be handled in General option classes 
# inlcluding some extra process such as data fetching, checking other options
# Use General option classes if there is no such extra process.

#
# airport option that is shown in textfield
# 
class Airport < ValueOption
  def initialize(name, outlet, items=nil, default='KSFO')
    @default = default
    super(name, outlet, default)
  end
  
  def key()
    if (AirportDictionary.instance.searchForCode(@value.to_s).first.city.to_s.downcase == 'carrier')
      return 'carrier'
    else
      return 'airport'
    end
  end

  def setValue(value)
    @value = value
    begin
      @current = AirportDictionary.instance.valueForCode(value)
      @outlet.setStringValue(value + ": " + @current.name())
    rescue
      # maybe bad airport code is in the preference file, trying to recover by specifying the default airport
      if (@value != @default) 
        @value = @default
        retry
      else
        # some unknown error occurs, trying to resolve by specifying "" into Airport text field
	if (@outlet)
          @outlet.setStringValue("")
        else
          puts "cannot find outlet for #{self.class},#{@value}"
        end
      end
    end
  end

  def setInitialValue()
    setValue(@initValue)
  end
  
  def update()
    return if (!@outlet)
    @current = AirportDictionary.instance.valueForCode(@outlet.stringValue().to_s.split(/: /).first)
    @value = @current.code if (@current)
  end

end

#
# aircraft option that is shown in textfield
# 
class Aircraft < ValueOption
  def initialize(name, outlet, items=nil, default='c172p')
    @dictionary = AircraftDictionary.instance
    @default = default
    super(name, outlet, default)
  end

  def reload()
    @dictionary.reload()
    @current = @dictionary.valueForCode(@value)
    @outlet.setStringValue(@current.name)
  end

  def currentImage()
    return @current.getImage()
  end

  def setValue(value)
    @value = value
    begin
      @current = @dictionary.valueForCode(@value)
      @outlet.setStringValue(@current.stringValue())
    rescue
      @value = @default
      @current = @dictionary.valueForCode(@value)
      if (@current)
        @outlet.setStringValue(@current.stringValue()) if (@outlet)
      else
        @outlet.setStringValue("") if (@outlet)
      end
    end
  end

  def setInitialValue()
    setValue(@initValue)
  end

  def update()
    return if (!@outlet)
    @current = @dictionary.valueForName(@outlet.stringValue().to_s)
    @value = @current.code if (@current)
  end
end

#
# "Download scenery on the fly" option 
#
class TerraSync < BoolOption
  def initialize(name, outlet, postfix=nil, default=false)
    @item = ['--enable-terrasync']
    super(name, outlet, postfix, default)
  end

  def generate()
    return @item if (@value == true)
    return '--disable-terrasync'
  end
end

#
# Atlas Map
#
class Atlas < BoolOption
  def initialize(name, outlet, postfix=nil, default=false)
    super(name, outlet, postfix, default)
    @item="--nmea=socket,out,1,localhost,5050,udp"
  end

  def generate()
    return @item if (@value == true)
    return ''
  end
end

#
# For options that are not implemented on this GUI 
#
class CommandLineOptions < ValueOption
  def initialize(name, outlet, default='')
    super(name, outlet, default)
  end

  def generate()
    if (@value)
      save()
      return @value.split(/\s/)
    else
      return ''
    end
  end
end

=begin
class Path < ValueOption
  def key()
    return 'fg-root'
  end
  
  def initialize(name, outlet=nil, default=nil)
    @name = name.to_s
    @value = NSBundle.mainBundle.resourcePath.to_s.gsub(/ /, '\ ');
  end

  attr_reader :value
end
=end

class Defaults < ValueOption
  def generate()
    path = NSBundle.mainBundle.resourcePath.to_s.gsub(/ /, '\ ');
    return []
  end
end

class AntiAliasingOption < SliderOption
  def generate()
    save()
    if (@value > 0)
      return "--prop:/sim/rendering/multi-sample-buffers=1 --prop:/sim/rendering/multi-samples=#{@value + 1}"
    else
      return ""
    end
  end
end

#
# options for Multi-player mode
#
class MultiPlay < BoolOption
  def initialize(name, outlet, default=false)
    @@active = false
    @initValue = false
    super(name, outlet, default)
  end
  
  def self.enabled?()
    if (defined?(@@active))
      return @@active
    else
      return false
    end
  end

  def setValue(value)
    super(value)
    @@active = @value
  end
  
  def update()
    super()
    @@active = @value
  end
  
  def generate()
    save()
    return ''
  end
end

class Server < ValueOption
  OUTGOING='out'
  def initialize(name, outlet, default=nil)
    if (name == 'server')
      @direction = OUTGOING
      default = 'mpserver02.flightgear.org'
      @initValue = @address = default
    end
    # will be changable at UI control soon, hopefully
    @port = 5000
    super(name, outlet, default)
  end

  def generate()
    update()
    if (@name == 'server' && @value != @default)
      save()
    end
    if (MultiPlay.enabled?())
      return "--multiplay=#{@direction},10,#{@value},#{@port}" 
    else
      return ''
    end
  end
  attr_reader :port, :address
end

class CallSign < ValueOption
  def initialize(name, outlet = nil, default = nil)
    super(name, outlet, ENV['USER'])
  end
  
  def generate()
    save()
    @value.gsub!(/ /, '_')
    @value = @value[0..6]
    if (MultiPlay.enabled?)
      `open http://mpmap02.flightgear.org/?follow=#{@value}`
      return "--callsign=#{@value}"
    else
      return ''
    end
  end
end

class FGComServer < ValueOption
  def initialize(name, outlet = nil, default = "fgcom.flightgear.org")
    @initValue = default
    super(name, outlet, default)
  end

  def setValue(value)
    @value = value
  end

  def generate()
    save()
    return ""
  end
end

class FGComMode < SelectableOption
    def initialize(name, outlet = nil, items = ['Off', 'Built-in', 'Standalone'], default='Off')
        @initValue = default
        super(name, outlet, items, default)
        @index = @outlet.indexOfSelectedItem()
    end
    
    def generate()
        save()
        @index = @outlet.indexOfSelectedItem()
        if (MultiPlay.enabled? && @index > 0)
            if (@index == 1)
                return "--enable-fgcom"
            else
                return "--generic=socket,out,2,127.0.0.1,16661,udp,fgcom"
            end
        end
        # force FGCom to off (since state is auto-saved)
        return "--disable-fgcom"
    end
    
    attr_reader :index
end
