#
#  FlightGearController.rb
#  FlightGearMacOSX
#
#  Created by Tatsuhiro Nishioka on 11/23/06.
#  Copyright (c) 2006 Tatsuhiro Nishioka
# 
#  http://macflightgear.sourceforge.net/
# 
#  This file is part of FlightGearMacOSX.
#
#  FlightGearMacOSX is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  FlightGearMacOSX is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with FlightGearMacOSX. If not, see <http://www.gnu.org/licenses/>
#

require 'osx/cocoa'
require 'shell'
require 'OptionParser'
require 'Options'
require 'Preferences'
require 'RenderingOptions'
require 'PositionOptions'

include OSX

# Launching commands by Ruby Thread.
# Make sure there's no Cocoa calls within Threads
class Launcher
  def initialize(path, command, args, background=false)
    @command = command
    @path = path.gsub(/ /, '\ ')
    @args = args.flatten.delete_if {|arg| arg == ''}
    @background = background
    puts "Starting #{command} with #{@args.join(' ')}"
    sh = Shell.cd(path)
    params = ["#{path}/#{command}"] + @args
      
  @thread = Thread.new {
      if (@background == true)
        @cmd = sh.system(*params)
        sh.out(@cmd)
      else
        sh.out {sh.system(*params)}
      end
      }
  end
  
  def join()
    @thread.join
    puts "Exiting #{File.basename(@command)}"
=begin
    # Copying .ppm files are disabled for performance reason. 
    # this code delays a few seconds (or several seconds) when quitting fgfs
    # (plus Mac can't read it by default anyway)

    # Moves all screenshot images to ScreenShots folder
    screenShotDir = @path + '/data/ScreenShots'
    begin
      if (`find #{@path} -name "*.ppm"`.size > 0)
        `cp "#{@path}/*.ppm" #{screenShotDir}`
      end
    rescue Errno::ENOENT
      p $!
    end
=end
  end
  
  def kill()
    puts "Exiting #{File.basename(@command)}"
    if (@background)
      @cmd.kill(9)
    else
      @thread.kill
    end
  end
end

#
# Main FlightGearMacOSX GUI Controller
#
class FlightGearController < NSObject
  include OptionHolder

  ib_outlets :aircraft, :airport, :terrasync, :image, :advanced, :tab, 
             :aircraftButton, :airportButton, :icon, :atlas
  
  def awakeFromNib()
    @@inst = self
    advanced = false
    @advanced.setState((advanced != nil)? advanced : false);
    toggleAdvancedOptions() if (advanced == 1)
    bindings = {
      :aircraft => Aircraft, 
      :airport => Airport, 
      :terrasync => TerraSync, 
      :arch => [BoolOption, '', false], 
      :default => Defaults,
      :atlas => Atlas}

    @parser = OptionParser.new(self, bindings)
    @path = NSBundle.mainBundle.resourcePath.to_s
    @macOSPath = NSBundle.mainBundle.bundlePath.to_s + "/Contents/MacOS"
    
    notifier = OSX::NSNotificationCenter.defaultCenter
    {'launcherWillTerminate:' => OSX::NSApplicationWillTerminateNotification,
      'windowDidBecomeKeyNotification:' => OSX::NSWindowDidBecomeKeyNotification,
      'reloadAircraft:' => 'FGReloadAircraftNotification',
      'selectAircraft:' => 'FGAircraftDidSelectNotification',
    }.each do |selector, name|
      notifier.addObserver_selector_name_object(self, selector, name, nil)
    end

    ObjectSpace.each_object(PositionOptions) {|object| object.entry = @airport }
    ObjectSpace.each_object(AircraftOptions) {|object| object.entry = @aircraft }
  end
  
  def selectAircraft(sender = nil)
    @parser.update('aircraft')
    imagePath = @parser.options['aircraft'].currentImage
    @image.setImage(NSImage.alloc.initByReferencingFile(imagePath)) if (imagePath)
  end
  
  def openTab(id)
    @tab.selectTabViewItemWithIdentifier(id)
    if (@advanced.state.to_i == 0)
      @advanced.setState(1)
      toggleAdvancedOptions(nil) 
    end
  end
  
  def openPositionTab(sender = nil)
    openTab('PositionTab')
  end
  
  def openAircraftTab(sender = nil)
    openTab('AircraftTab')
  end

  def startFlight(sender = nil)
    args = []

    ObjectSpace.each_object(OptionParser) do |options| 
      args << options.parse()
      @networkParser = options if options.options['fgcomMode']
    end
    
    Preferences.synchronize()
    localizeFGMenu()

    # JMT - commenting-out terrasync dir, let's use the default location
    # in Library/Application Support
    #if (@parser.options['terrasync'].value)
    #  terraSyncDir=@path + '/data/Scenery-TerraSync'
    #  Dir.mkdir(terraSyncDir) if (!File.exists?(terraSyncDir))
    #end
    
    if (@parser.options['atlas'].value)
      @atlasThread = Launcher.new(@path, 'Atlas', 
		["--fg-root=./data", 
#                 "--fg-scenery=./data/Scenery-TerraSync",
                 "--palette=./data/Atlas/Palettes/default.ap",
                 "--udp=5050", "--atlas=./data/Atlas", "--autocenter-mode"], true);
    end
    
    if (@networkParser.options['isMultiplay'].value == true)
      fgcom = @networkParser.options['fgcomMode'].index
      fgcomServer = @networkParser.options['fgcomServer'].value
      if (fgcom == 2 && fgcomServer.size > 0)
        @fgcomThread = Launcher.new(@macOSPath, 'fgcom', ["-S#{fgcomServer}"], true)
      end
    end
    
    @flightGearThread = Launcher.new(@path, 'fgfs.sh', args.flatten)
    @flightGearThread.join
#    @terrasyncThread.kill if (defined?(@terrasyncThread))
    @fgcomThread.kill if (defined?(@fgcomThread))
    @atlasThread.kill if (defined?(@atlasThread))
  end

  def localizeFGMenu()
    currentLocalization = NSBundle.mainBundle.preferredLocalizations.to_a.first

    srcPath = @path + "/" + currentLocalization + ".lproj/localData"
    if (!FileTest.exist?(srcPath))
      srcPath = @path + "/English.lproj/localData"
    end
    if (FileTest.exist?(srcPath))
      destPath = @path + "/data"
      `cp -R "#{srcPath}/" "#{destPath}"`
    end
  end

  def showHelp(sender = nil)
    system("open http://macflightgear.sourceforge.net/home/documents/")
  end

  def showHelpSystem(sender = nil)
	system("open #{NSBundle.mainBundle.resourcePath.to_s}/data/Docs/index.html")
  end
  
  def showOnlineManual(sender = nil)
	system("open #{NSBundle.mainBundle.resourcePath.to_s}/data/Docs/getstart.pdf")
  end
  
  def quit(sender = nil)
    OSX::NSApp.terminate(self)
    return true
  end

  def windowShouldClose(sender = nil)
    quit(sender)
  end

  def toggleAdvancedOptions(sender = nil)
    window = OSX::NSApp.keyWindow
    frame=window.frame;
    delta = (@advanced.state.to_i == 1)? 1 : -1
    height = @tab.frame.height + 8; # needs some margin, say 8 at this moment
    frame.origin.y -= height * delta
    frame.size.height += height * delta
    window.setFrame_display_animate(frame, true, true)
  end

  def reloadAircraft(notification=nil, object=nil, info=nil)
    @parser.reload(:aircraft)
  end

  def windowDidBecomeKeyNotification(notification=nil, object=nil, info=nil)
    # open the advanced features tab if it is enabled.
    toggleAdvancedOptions() if (@advanced.state.to_i == 1)
    NSNotificationCenter.defaultCenter.removeObserver_name_object(self, NSWindowDidBecomeKeyNotification, nil)
  end
  
  def launcherWillTerminate(notification=nil)
    # save all the instances of Option
    ObjectSpace.each_object(Option) {|option| option.update(); option.save()}
    Preferences.setValue('advanced', @advanced.state.to_i)
    Preferences.synchronize()
    NSNotificationCenter.defaultCenter.removeObserver(self)
  end
end
