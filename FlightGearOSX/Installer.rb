#
#  Installer.rb
#  FlightGearOSX
#
#  Created by Tatsuhiro Nishioka on 11/27/06.
#  Copyright (c) 2006 Tatsuhiro Nishioka.
#  http://macflightgear.sourceforge.net/
# 
#  This file is part of FlightGearMacOSX.
#
#  FlightGearMacOSX is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  FlightGearMacOSX is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with FlightGearMacOSX. If not, see <http://www.gnu.org/licenses/>
#

require 'osx/cocoa'
require 'shell'
require 'fileutils'

include OSX

#
# Various Installer
#
class Installer
  # check if only contents of the src folder is needed
  def needContentsOnly?(src, dest)
    return false if (!FileTest.directory?(src))
    return false if (dest == rootDir() + "/Aircraft")
    return false if (dest == resourceDir() && src =~ /\.lproj$/)
    return true if (dest == rootDir() + "/Scenery")
    return true
  end

  def initialize(entity, dest)
    @entity = entity
    if (dest.is_a?(String) && dest !~ /^\//) # check if it is absolute path
      @dest = rootDir() + "/#{dest}"
    else
      @dest = dest
    end
  end
  
  def resourceDir()  
    return NSBundle.mainBundle.resourcePath.to_s
  end

  def rootDir()
    return NSBundle.mainBundle.resourcePath.to_s + '/data'
  end
end

class FolderInstaller < Installer
  def install()
    result = ""
    if (@dest.is_a?(Hash))
      # will copy each file to its destination (hash has pairs of a file and its destination)
      @dest.each do |file, dest|
        file = "/tmp/FGInstall/#{file}" if (@entity == "/tmp/FGInstall")
        file += "/" if (needContentsOnly?(file, dest))
        result += `cp -RL "#{file}" "#{dest}" 2>&1 `.chomp
# puts "cp -R #{file} #{dest} 2>&1"
      end
    else
      # will copy the contents of the folder or a single file
      if (@entity == '/tmp/FGInstall')
        contents = `ls #{@entity}/`.split(/\n/)
        @entity += "/" + contents[0] if (contents.size == 1)
      end
      # FIXME: it is now just for Scenery, but should be extended using dest filter or something.
      @entity += "/" if (needContentsOnly?(@entity, @dest))
      result = `cp -RL "#{@entity}" "#{@dest}" 2>&1`.chomp
# puts "cp -R #{@entity} #{@dest} 2>&1"
    end
    raise result if (result.length > 0)
  end
end

class ArchiveInstaller < Installer
  def initialize(entity, command, dest)
    super(entity, dest)
    @command = command
  end

  def install()
    raise NSLocalizedString("Unknown file type").to_s + " - #{File.extname(@entity)}" if (!@command)
    sh = Shell.cd(@dest)
    commands = @command.split(/ /)
    commands << @entity
    sh.transact { system(*commands) }
#puts commands.join(" ");
  end
end


class InstallerFactory
  @@destinations = {}
  Extractor = {'.tar' => 'tar xf', '.tgz' => 'tar xzf', '.gz' => 'tar xzf', '.zip' => 'unzip -o', '.bz2' => 'tar xjf', '.7z' => '7za x'}

  Viewer = {
    '.tar' => ['tar tf',  Proc.new {|buffer| buffer.split(/\n/) }],
    '.tgz' => ['tar tzf', Proc.new {|buffer| buffer.split(/\n/) }],
    '.gz'  => ['tar tzf', Proc.new {|buffer| buffer.split(/\n/) }],
    '.bz2' => ['tar tjf', Proc.new {|buffer| buffer.split(/\n/) }],
    '.7z'  => ['7za l',   Proc.new {|buffer| list = []; buffer.each {|line| if (line =~ /[0-9]:[0-9]/); list << line.split(/ /).last.chop; end }; list }],
    '.zip' => ['unzip -l', Proc.new {|buffer| list = []; buffer.each {|line| if (line =~ /[0-9]:[0-9]/); list << line.split(/ /).last.chop; end }; list }]
  }
  
  TMP_FOLDER='/tmp/FGInstall'

  def self.clearDestinations()
    @@destinations = {}
  end

  def self.extractor(entity)
    return Extractor[File.extname(entity)]
  end

  def self.viewer(entity)
    return Viewer[File.extname(entity)]
  end

  def self.listing(entry)
    list = []
    command, block = self.viewer(entry)
    return nil if command == nil
    begin
      result = `#{command} #{entry}`
      list = block.call(result)
      raise if (list.size == 0 || ($?.exited?() && $?.exitstatus() != 0))
    rescue
      puts $!
      raise NSLocalizedString("Error while reading contents").to_s
    end
    return list
  end

  def self.create(entry)
    begin
      if (FileTest.directory?(entry))
        listing = `find #{entry} -name "*"`.split(/\n/)
        listing.delete_if {|file| file == entry}
      else
        begin
          # first, we assume it is an archive file
          listing = self.listing(entry)
          listing = [File.basename(entry)] if (!listing)
        rescue
          # in case we got archive error, we assume it is a regular file
          raise NSLocalizedString("Not a valid file or folder").to_s + ": #{entry}"
        end
      end

      @@destinations[File.basename(entry)] = destinations = TypeDetector.findDestinations(listing)
      extractor = self.extractor(entry);
      if (extractor)
        `rm -rf #{TMP_FOLDER}`
        `mkdir -p #{TMP_FOLDER}`
        installer = ArchiveInstaller.new(entry, extractor, TMP_FOLDER);
        installer.install()
        return FolderInstaller.new(TMP_FOLDER, destinations) if (extractor)
      end

      if (destinations.is_a?(Hash))
        return FolderInstaller.new(listing, destinations)
      else 
        return FolderInstaller.new(entry, destinations)
      end
    end
  end

  def self.destinations()
    resourcePath = NSBundle.mainBundle.resourcePath.to_s + '/'
    appPath = NSBundle.mainBundle.resourcePath.to_s.split('/')[0..-2].join("/") + "/"
    if (@@destinations.is_a?(Hash))
      list = []
      @@destinations.each do |file, dest|
	if (list.size < 10)
          dest = NSLocalizedString("(multiple destination)") if (dest.is_a?(Hash))
          list << "#{file} >> #{dest.gsub(resourcePath, '')}"
        end
      end
      list << NSLocalizedString(".... (suppressed)") if (@@destinations.size > 10)
      return list.join("\n");
    else
      return @@destinations.gsub(resourcePath, '')
    end
  end
end

#
# Detectors
# Detector classes finds where to install a given archive / folder / single file
# If there are multiple destination folders, a class returns hash in the format of
# {file1 => destination1, file2 => destination2, ... }
#
class NullDetector
  def matched?(list)
    raise NSLocalizedString("Invalid file or unknown destination.").to_s
  end
  
  alias destination matched?
end

class AircraftDetector
  def matched?(list)
    list.each {|file| return true if (File.basename(file) =~ /-set\.xml$/) }
    return false
  end

  def destination()
    return NSBundle.mainBundle.resourcePath.to_s + "/data/Aircraft"
  end
end

class SceneryDetector
  def matched?(list)
    hasObject = hasTerrain = hasTileDir = false
    list.each do |file|
      hasObject = true if (File.dirname(file) =~ /Objects/)
      hasTerrain = true if (File.dirname(file) =~ /Terrain/)
      hasTileDir = true if (File.dirname(file) =~ /[ew][0-9]{3}[ns][0-9]{2}$/)
      return true if ((hasObject || hasTerrain) && hasTileDir)
    end
    return ((hasObject || hasTerrain) && hasTileDir)
  end

  def destination()
    return NSBundle.mainBundle.resourcePath.to_s + "/data/Scenery"
  end
end

# this needs to restart 
class GUIUpdateDetector
  def matched?(list)
    list.each {|file| return true if (File.basename(file) =~ /GUIUpdate$|GUIUpdater$/) }
    return false
  end
  
  def destination()
    return NSBundle.mainBundle.bundlePath.to_s + "/Contents"
  end
end

class AddOnDetector
  Candidates = ["fgcom", "Atlas", "terrasync", "plugins", "Map", "MapPS", "fgjs", "js_demo"];
  def matched?(list)
    list.each {|file| return true if (Candidates.include?(File.basename(file)))}
    return false
  end

  def destination()
    return NSBundle.mainBundle.resourcePath.to_s
  end
end

#
# Any update materials under data folder
#
class FGBasePackageDetector
  def initialize()
    @destinations = {}
  end
  def matched?(list)
    rootDir = NSBundle.mainBundle.resourcePath.to_s + '/data'
    dataFiles = `find #{rootDir} -name "*"`.split(/\n/)
    list.each do |file|
      matched = false
      next if (file =~ /\/$/)
      dataFiles.each do |existing|
        next if (matched)
        existing_short = existing.gsub(rootDir + "/", '')
        existing_with_parent_folder = existing_short.split(/\//)[-2..-1]
        if (existing_with_parent_folder) 
          existing_shorter = existing_with_parent_folder.join("/")
        else
          existing_shorter = nil
        end
        if (file == existing_short)
          @destinations[file] = File.dirname(existing)
          matched = true
        elsif (file == existing_shorter)
          @destinations[file] = File.dirname(existing)
          matched = true
        elsif (File.basename(file) == File.basename(existing) && File.basename(file).size > 0)
          if (!@destinations[file])
            @destinations[file] = File.dirname(existing)
          elsif (!matched)
            raise file + " " + NSLocalizedString("has multiple destinations")
#            @destinations[file] = NSLocalizedString("Multiple destinations")
          end
        end
      end
    end
    return true if (@destinations.size == list.size)
    return false
  end

  def destination()
    # returns one destination if all destinations are the same 
    # otherwise, returns hash that contains pairs of file and destination
    dests = @destinations.values
    return dests.first if (dests.uniq.size == 1)
    return @destinations
  end
end

class PerPatternDetector < FGBasePackageDetector
  Patterns = {/\.nas$/ => "Contents/Resources/data/Nasal", 
              /_demo\.xml$/ => "Contents/Resources/data/AI", 
              /DavePack/ => "Contents/Resources/data/Aircraft",
              /Info.plist/ => "Contents",
              /\.rb$/ => "Contents/Resources",
              /FlightGear$/ => "Contents/MacOS",
              /\.lproj/ => "Contents/Resources"}

  def matched?(list) 
    resourcePath = NSBundle.mainBundle.bundlePath.to_s 
    list.each do |file|
      Patterns.each do |pattern, dest| 
        if (file =~ pattern)
          @destinations[file] = resourcePath + "/" + dest
        end
      end
    end
    return true if (@destinations.size == list.size)
    return false
  end
end
  

class ModelDetector
  Candidates = ["Agriculture", "Aircraft", "Airport", "Boundaries", 
               "Commercial", "Communications", "Industrial", "Military", 
               "Misc", "Power", "Residential", "Sport", "Transport", "Trees"]

  def matched?(list)
    matchnum = 0
    list.each do |file|
      Candidates.each {|folder| matchnum += 1 if (File.dirname(file) == folder) }
    end
    if (matchnum >= Candidates.size)
      return true
    else
      return false
    end
  end

  def destination
    return NSBundle.mainBundle.resourcePath.to_s + "/data/Models"
  end
end

class FrameworkDetector
  def matched?(list)
    list.each do |file|
      return true if (File.dirname(file) =~ /\.framework$/)
    end
    return false
  end

  def destination
    return NSBundle.mainBundle.resourcePath.to_s.split(/\//)[0..-2].join("/")+"/Frameworks"
  end
end

class PluginDetector
  def matched?(list)
    list.each do |file|
      return true if (File.basename(file) =~ /\.so$/)
    end
    return false
  end

  def destination
    return NSBundle.mainBundle.resourcePath.to_s + "/plugins"
  end
end

class TypeDetector
  Detectors = [AircraftDetector, SceneryDetector, GUIUpdateDetector, ModelDetector, AddOnDetector, 
               FrameworkDetector, PluginDetector, PerPatternDetector, FGBasePackageDetector,
               NullDetector]
  def self.findDestinations(listing)
    Detectors.each do |klass|
      detector = klass.new
      if (detector.matched?(listing))
        return detector.destination()
      end
    end
    return []
  end
end

=begin
installer = InstallerFactory.create(ARGV[0])
installer.install
puts "Installed: #{ARGV[0]} to #{InstallerFactory.destinations()}"
=end
