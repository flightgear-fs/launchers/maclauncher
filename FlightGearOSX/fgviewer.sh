#!/bin/sh

if [ ! -L "$PWD/data/Fonts/arial.ttf" ]; then
  ln -s /System/Library/Fonts/AppleGothic.ttf "$PWD/data/Fonts/arial.ttf"
fi
export FG_ROOT=$PWD/../Resources/data
../MacOS/fgviewer $*

