#
#  GeneralOptions.rb
#  FlightGearOSX
#
#  Created by Tatsuhiro Nishioka on 11/28/06.
#  Copyright (c) 2006 Tatsuhiro Nishioka
#  http://macflightgear.sourceforge.net/
# 
#  This file is part of FlightGearMacOSX.
#
#  FlightGearMacOSX is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  FlightGearMacOSX is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with FlightGearMacOSX. If not, see <http://www.gnu.org/licenses/>
#


require 'osx/cocoa'
require 'OptionParser'
#
# Controller for General options
#
class GeneralOptions < OSX::NSObject
  include OptionHolder

  ib_outlets :saveOnExit, :units, :control, :timeOfDay, :season, :logLevel

  LOG_FILE = "#{ENV['HOME']}/Library/Application Support/FlightGear/log.txt"

  def awakeFromNib()
    @bindings = {:saveOnExit => [BoolOption, 'save-on-exit', true],
      :units => [SwitchableOption, ['feet', 'meters'], 'feet'],
      :control => [SelectableOption, ['auto', 'keyboard', 'joystick', 'mouse'], 'auto'],
      :timeOfDay => [SelectableOption, ['real','dawn','morning','noon','afternoon','dusk','evening','midnight'], 'real'],
      :season => [SelectableOption, ['summer', 'winter'], 'summer'],
      :logLevel => [SelectableOption, ['none', 'warn', 'info', 'debug', 'bulk'], 'none']}
    @parser = OptionParser.new(self, @bindings)
# p @parser.options
    @parser.options['logLevel'].setName('log-level')
  end
  
  def viewLog(sender = nil)
    `open -e "#{LOG_FILE}"`
  end
end
