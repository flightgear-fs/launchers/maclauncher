#
#  Preferences.rb
#  FlightGearOSX
#
#  Created by Tatsuhiro Nishioka on 11/26/06.
#  Copyright (c) 2006 Tatsuhiro Nishioka
#  http://macflightgear.sourceforge.net/
# 
#  This file is part of FlightGearMacOSX.
#
#  FlightGearMacOSX is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  FlightGearMacOSX is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with FlightGearMacOSX. If not, see <http://www.gnu.org/licenses/>
#

require 'singleton'

include OSX
#
# Preferences for FlightGearMacOSX (GUI laungher)
#
class Preferences
  include Singleton
  def init()
    @domain = NSBundle.mainBundle.objectForInfoDictionaryKey('CFBundleIdentifier').to_s
    @defaults = NSUserDefaults.standardUserDefaults
    @dictionary = @defaults.persistentDomainForName(@domain)
    @preferences = NSMutableDictionary.alloc.init
    @preferences.addEntriesFromDictionary(@dictionary) if (@dictionary)
#    checkImageDir()
  end

#  def checkImageDir()
#    bundle = NSBundle.mainBundle
#    imageDir = NSBundle.mainBundle.resourcePath + '/data/ScreenShots'
#    Dir.mkdir(imageDir) if (!File.exists?(imageDir))
#  end
  
  def _synchronize()
    init() if (!defined?(@defaults))
    @defaults.setPersistentDomain_forName(@preferences, @domain)
    @defaults.synchronize()
  end

  def _valueForKey(key)
    init() if (!defined?(@defaults))
    return @preferences.valueForKey(key) if (@preferences)
  end

  def _setValue(key, value)
    init() if (!defined?(@defaults))
    return @preferences.setValue_forKey(value, key)
  end
  
  def self.synchronize()
    instance._synchronize()
  end
  
  def self.valueForKey(key)
    instance._valueForKey(key)
  end
  
  def self.setValue(key, value)
    instance._setValue(key, value)
  end	
end
