#
#  FavoriteDictionary.rb
#  FlightGearOSX
#
#  Created by Tatsuhiro Nishioka on 01/16/2008.
#  Copyright (c) 2008 Tatsuhiro Nishioka
#  http://macflightgear.sourceforge.net/
#
#  This file is part of FlightGearMacOSX.
#
#  FlightGearMacOSX is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  FlightGearMacOSX is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with FlightGearMacOSX. If not, see <http://www.gnu.org/licenses/>
#

require 'Dictionary'
require 'osx/cocoa'
require 'singleton'

include OSX

#
# Favorite Entry
#
class FavoriteEntry < NSObject
  def initWithDictionary(key, values)
    @key = key.to_s
    @values = values
    return self
  end

  def name()
    return @key
  end

  def getSearchables()
    return [@values.values]
  end

  def stringValue()
    return @key
  end

  def airport()
    return @values["airport"]
  end
  
  def aircraft()
    return @values["aircraft"]
  end

  def to_a()
    return [@key, @values]
  end

  attr_reader :key, :values
end

#
# Data source for the TableView
#
class FavoriteDictionary < Dictionary
  def self.instance()
    if (!defined?(@@inst))
      @@inst = FavoriteDictionary.alloc.init()
      @@inst.load()
    end
    return @@inst
  end

  def load()
    dir=NSBundle.mainBundle.resourcePath
    @favoriteFile = dir + '/data/Favorites.rb'
    begin
      require @favoriteFile
    rescue LoadError
      $favorites=[]
    end

    super()

    $favorites=[] if (!defined?($favorites))
    $favorites.each do |entry|
      key, value = entry
      @entries << FavoriteEntry.alloc.initWithDictionary(key, value)
    end
  end	

  def add(name)
#    raise 'Name already in use' if ($favorites[name.to_s])
    data = {}
    ObjectSpace.each_object(Option) do |option|
      option.update()
      if option.value.kind_of?(NSCFString)
        data[option.keyName] = option.value.to_s
      else
        data[option.keyName] = option.value
      end
# puts "adding #{option.keyName} to #{option.value.to_s}"
    end
    @entries << FavoriteEntry.alloc.initWithDictionary(name, data)
  end

  def remove(index)
    name = @entries.delete_at(index)
  end

  def apply(index)
    return if (!defined?(@entries) || !@entries)
    data = @entries[index]
    ObjectSpace.each_object(Option) do |option|
# puts "set #{data.values[option.keyName.to_s]} to #{option}"
      option.setValue(data.values[option.keyName.to_s])
    end
  end 

  def save()
    begin
      buffer = File.new(@favoriteFile, 'w')
      buffer.write("$favorites=")
      $favorites = []
      @entries.each do |entry|
        $favorites << entry.to_a
      end
      buffer.write($favorites.inspect)
      buffer.write(";")
      buffer.close
    rescue Errno::ENOENT
      puts "Can't open favorite data file: favorites.rb"
    end
  end
end

