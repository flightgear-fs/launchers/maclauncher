#
#  Others.rb
#  FlightGearOSX
#
#  Created by Tatsuhiro Nishioka on 11/28/06.
#  Copyright (c) 2006 Tatsuhiro Nishioka.
#  http://macflightgear.sourceforge.net/
# 
#  This file is part of FlightGearMacOSX.
#
#  FlightGearMacOSX is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  FlightGearMacOSX is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with FlightGearMacOSX. If not, see <http://www.gnu.org/licenses/>
#


require 'osx/cocoa'
require 'OptionParser'
require 'Options'
require 'Installer'

include OSX

#
# Controller for Other Options that are on the "Others" tab
#
class OtherOptions < NSObject
  include OptionHolder

  ib_outlets :others

  def awakeFromNib()
    bindings = {:others => CommandLineOptions}
    @parser = OptionParser.new(self, bindings)
    @resourcePath = NSBundle.mainBundle.resourcePath.to_s
  end
  
  def viewOptions(sender = nil)
    `open "#{@resourcePath}/options.html"`
  end
  
  def install(sender = nil)
    errors = [];
    panel = NSOpenPanel.openPanel
    panel.setAllowsMultipleSelection(true)
    panel.setCanChooseDirectories(true)
    result = panel.runModalForDirectory_file_types(OSX.NSHomeDirectory, nil, nil)
    return if (result != NSOKButton)
    files = []
      InstallerFactory.clearDestinations();
      panel.filenames.to_a.each do |file|
      begin
        @file = file
        installer = InstallerFactory.create(file.to_s)
        raise NSLocalizedString('Not a valid data file / folder').to_s if (!installer)
        installer.install()
        files << File.basename(file.to_s)
      rescue
        NSRunAlertPanel(NSLocalizedString("Installation error").to_s, 
                        NSLocalizedString("Error occurs during installing the following file:\n").to_s +
                        @file + NSLocalizedString("\nError message is:\n").to_s +
                        " #{$!.message}\n",
                        nil,nil,nil)
        errors << @file
      end
    end

    message = ""
    if (files.size > 0)
      message = NSLocalizedString("The following files/directories are successfully installed\n") + InstallerFactory.destinations()
    end
    if (errors.size > 0)
      message += "\n\n" + NSLocalizedString("The folloing files are not installed").to_s + ":\n" + errors.join("\n")
    end
    
    NSRunInformationalAlertPanel(NSLocalizedString("Installation completed").to_s, 
                                 message, nil, nil, nil)
    NSNotificationCenter.defaultCenter.postNotificationName_object('FGReloadAircraftNotification', self)
  end	

  def openDataFolder(sender = nil)
    puts `open "#{@resourcePath}/data" 2>&1`
  end
end
