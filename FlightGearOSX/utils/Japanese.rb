
$resourceDictionary['Japanese'] = {
# Main Pane
'Aircraft' => "機体",
'Airport' => "空港",
'Download scenery on the fly' => "風景の自動ダウンロード",
'Navigation Map (Atlas)' => "ナビゲーションマップ (Atlas)",
'Start Flight' => "飛行開始",
'Quit' => "終了",
'Advanced Features' => "拡張機能",

# Tabs
'General' => "一般",
'Features' => "機能",
'Rendering' => "描画",
'Favorite' => "お気に入り",
'Position' => "位置",
#'Aircraft' => "機体",
'Network' => "ネットワーク",
'Others' => "その他",

# Genral Tab
'Save preferences on exit' => "終了時に設定を保存",
'Control' => "コントロール",
'Unit' => "単位",
'Time of day' => "時刻",
'Season' => "季節",
'Log level' => "ログレベル",
'View Log' => "ログ閲覧",

# Features Tab
'Overall' => "全般",
'Environment' => "環境",
'Head Up Display' => "ヘッドアップディスプレイ",
'Sound' => "サウンド",
'Instrument panel' => "計器パネル",
'Random objects' => "ランダムオブジェクト",
'AI models' => "AI モデル",
'No fuel consumption' => "燃料を消費しない",
'Start in a frozen state' => "停止状態で開始する",
'Clock never advances' => "時計を止める",
'Real weather fetch' => "実際の天気を取得",
'Horizon effect' => "地平線効果",
'Clouds' => "雲",
'3D clouds' => "3D 雲",
'Turbulence (0.0-1.0)' => "乱気流 (0.0-1.0)",
'Visibility (meter)' => "視界 (メートル)",
'Display HUD' => "HUD を表示",
'3D HUD (if available)' => "3D HUD (可能なら)",
'HUD with triangles' => "HUD with triangles",
'HUD shows % of triangles culled' => "HUD shows % of triangles culled",

# Rendering Tab
'Fullscreen mode' => "フルスクリーンモード",
'Window Size' => "ウインドウサイズ",
'Sky blending' => "スカイブレンディング",
'Textures' => "テキスチャ",
'Wireframe' => "ワイヤーフレーム",
'Fog' => "霧",
'Shading' => "シェーディング",

# Favorite tab
'You can add a set of current options with "+" on the top pane or below. Double click a row to restore the set of all options associated with it. ' => "画面左上又は下の「＋」ボタンを押すと、現在の設定を保存できます。保存した設定はダブルクリックすることで復元できます。",
'Name' => "名称",
#'Airport' => "空港",
#'Aircraft' => "機体",

#Position Tab
'Airport &amp; Runway' => "空港・滑走路",
'code, name, country...' => "コード, 空港名, 国名...",
'Code' => "コード",
#'Name' => "名称",
'Country' => "国",
'Filter' => "フィルタ",
'Runway' => "滑走路",
'Parking Position' => "駐機場",

#Aircraft Tab
# 'Filter' => "Filtre",
'description, FDM, status...' => "説明, FDM, 開発状況...",
'Description' => "名称・説明",
'FDM' => "FDM",
'Status' => "開発状況",
'Get Aircraft' => "機体を取得",
'Preview' => 'プレビュー',

# Network
'Multi Player' => "マルチプレーヤー",
'Enable Multiplay (through the network)' => "マルチプレーヤモードを有効にする(ネットワーク経由)",
'CallSign' => "コールサイン",
'Server' => "サーバー名/IP",
'Port' => "ポート",
'Your Mac' => "ローカルホスト名/IP",
'Voice ATS (FGCOM) on Multi Player mode' => "音声 ATC (FGCOM) - マルチプレーヤー時に有効",
'FGCOM provides a real voice ATC so you can talk to your wing mates while flying in multiplayer mode.' => "FGCOM を利用すると、マルチプレーヤモード時に音声でのチャットや航空管制(ATC) が可能になります。",
'Enable FGCOM in Multi Player mode' => "マルチプレーヤモード時にFGCOM を有効にする",
'FGCOM Server' => "FGCOM サーバ",

# Others
'Command-line options' => "コマンドラインオプション",
'Specify space-separated options (e.g. --key1=val1 --key2=val2)' => "オプションはスペースで区切ってください (例: --key1=val1 --key2=val2)",
'View Options' => "オプションを表示",
'Data Add-ons' => "追加データ",
'Select aircraft / scenery files and/or folders to install' => "追加する機体や風景を選択してインストールできます。",
'Install Add-on Data' => "データをインストール",
'Open data Folder' => "データフォルダを開く",

# Menu Items
# FlightGear
'FlightGear' => "FlightGear",
'About FlightGear Mac OS X' => "FlightGear Mac OS X について...",
'Preferences...' => "設定...",
'Services' => "サービス",
'Hide FlightGear Mac OS X' => "FlightGear Mac OS X を隠す",
'Hide Others' => "他を隠す",
'Show All' => "全てを表示",
'Quit FlightGear Mac OS X' => "FlightGear Mac OS X を終了",

# Edit
'Edit' => "編集",
'Undo' => "元に戻す",
'Redo' => "やり直し",
'Cut' => "カット",
'Copy' => "コピー",
'Paste' => "ペースト",
'Delete' => "削除",
'Select All' => "全て選択",

'Window' => "ウィンドウ",
'Minimize' => "最小化",
'Bring All to Front' => "全てを手前に移動",
'Help' => "ヘルプ",
'FlightGear Mac OS X Help' => "FlightGear Mac OS X ヘルプ",

# Dialog
'Type a name for a set of options.' => "お気に入りの名前を入力してください",
'Add' => "追加",
'Cancel' => "キャンセル"
}

