#!/usr/bin/ruby
$resourceDictionary = {};

require "Japanese"
require "French"

['Japanese', 'French'].each do |country|
  lines = []
  `cp ../English.lproj/MainMenu.nib/keyedobjects.nib /tmp`
  `plutil -convert xml1 /tmp/keyedobjects.nib`
  inFile = open("/tmp/keyedobjects.nib")
  outFile = open("keyedobjects.nib", "w")

  inFile.readlines.each do |line| 
    if (line =~ /<string>/ && line !~ /<string>NS\w+<\/string>/) 
      keyword = line.gsub(/^\s*/,'').gsub(/<string>/,'').gsub(/<\/string>/,'').chomp
        $resourceDictionary[country].each do |key,val| 
if (keyword == key)
          line2 = line.gsub(key,val) 
          line = line2
          break
        end
      end
    end
    lines << line
  end

  inFile.close()
  outFile.write(lines)
  outFile.close()

  `plutil -convert binary1 keyedobjects.nib`
  `cp ../English.lproj/MainMenu.nib/*.nib ../#{country}.lproj/MainMenu.nib/`
  `mv keyedobjects.nib ../#{country}.lproj/MainMenu.nib/`
end
