#!/usr/bin/ruby
# mkparkpos.rb: Generates ParkingPosition DataTable from Scenery File
# Usage: mkparkpos.rb <Scenery path> > ../ParkingPositions.rb
# 

require "rexml/document"

def getParkPos(filename)
  positions = []
  doc=REXML::Document.new(File.new(filename))
  doc.elements.each("groundnet/*/Parking") do |elem| 
    positions << elem.attributes["name"] + elem.attributes["number"] # if (elem.attributes["type"] == "gate" && elem.attributes["name"] != "Startup Location")
  end

  doc.elements.each("groundnet/*/Parking") do |elem| 
    positions << elem.attributes["name"] + elem.attributes["number"] # if (elem.attributes["type"] == "gate" && elem.attributes["name"] != "Startup Location")
  end

  return positions
end

if (!ARGV[0])
  puts "Usage: #{$0} <scenery path>"
  exit
end

puts <<EOD
#
# This is a cache for Parking positions written in FG_SCENERY/Airports/*/*/*/{parking.xml, groundnet.xml}
#
$parkingPositions = {
EOD

files = Dir.glob("#{ARGV[0]}/Airports/*/*/*/*.parking.xml") + Dir.glob("#{ARGV[0]}/Airports/*/*/*/*.groundnet.xml")
puts
files.each do |file|
  $stderr << "checking #{file}\n"
  puts '"' + File.basename(file).split(".").first + "\"=>[\"" + getParkPos(file).uniq.join("\", \"") + "\"],"
end

puts "}"
