
$resourceDictionary['French'] = {
# Main Pane
'Aircraft' => "Avion",
'Airport' => "Aéroport",
'Download scenery on the fly' => "Scénarios chargés à la volée",
'Navigation Map (Atlas)' => "Cartes de navigation (Atlas)",
'Start Flight' => "Lancer Flight",
'Quit' => "Quitter",
'Advanced Features' => "Configuration avancée",

# Tabs
'General' => "Générale",
'Features' => "Options", 
'Rendering' => "Aspect",
'Favorite' => "Favoris",
'Position' => "Aéroport",
#'Aircraft' => "Avion",
'Network' => "Réseau",
'Others' => "Autres",

# Genral Tab
'Save preferences on exit' => "Sauver préférences à la fermeture",
'Control' => "Contrôle",
'Unit' => "Unités",
'Time of day' => "Heure du jour",
'Season' => "Saison",
'Log level' => "Niveau Log",
'View Log' => "Voir Log",

# Features Tab
'Overall' => "Globales",
'Environment' => "Environnement",
'Head Up Display' => "Affichage Tête Haute (HUD)",
'Sound' => "Son",
'Instrument panel' => "Paneau d'instrument",
'Random objects' => "Objets aléatoires",
'AI models' => "modèles AI",
'No fuel consumption' => "Pas de conso. de carburant",
'Start in a frozen state' => "Démarrage à froid",
'Clock never advances' => "L'heure n'évolue pas",
'Real weather fetch' => "Météo réelle",
'Horizon effect' => "Effet d'horizon",
'Clouds' => "Nuages",
'3D clouds' => "Nuages 3D",
'Turbulence (0.0-1.0)' => "Turbulence (0.0-1.0)",
'Visibility (meter)' => "Visibilité (métres)",
'Display HUD' => "Affiche HUD",
'3D HUD (if available)' => "3D HUD (si autorisé)",
'HUD with triangles' => "HUD avec triangles",
'HUD shows % of triangles culled' => "HUD shows % of triangles culled",

# Rendering Tab
'Fullscreen mode' => "Plein écran",
'Window Size' => "Taille fenêtre",
'Sky blending' => "Lissage du ciel",
'Textures' => "Textures",
'Wireframe' => "Vue fil-de-fer",
'Fog' => "Brouillard",
'Shading' => "Ombrage",

# Favorite tab
'You can add a set of current options with "+" on the top pane or below. Double click a row to restore the set of all options associated with it. ' => "Ajouter un bloc d'options courantes avec le "+" situé ci-dessus ou ci-dessous. Double cliquer une ligne restore le bloc associé à la ligne.",
'Name' => "Nom",
#'Airport' => "Aéroport",
#'Aircraft' => "Avion",

#Position Tab
'Airport &amp; Runway' => "Aéroport &amp; Piste",
'code, name, country...' => "code, nom, pays...",
'Code' => "Code",
#'Name' => "Nom",
'Country' => "Pays",
'Filter' => "Filtre",
'Runway' => "Piste",
'Parking Position' => "Parking Position",

#Aircraft Tab
# 'Filter' => "Filtre",
'description, FDM, status...' => "description, FDM, status...",
'Description' => "Description",
'FDM' => "FDM",
'Status' => "Status",
'Get Aircraft' => "Charger avion",
'Preview' => 'Preview',

# Network
'Multi Player' => "Joueurs Multiples",
'Enable Multiplay (through the network)' => "Joueurs multiples (par le réseau)",
'CallSign' => "Identité",
'Server' => "Serveur",
'Port' => "Port",
'Your Mac' => "Votre Mac",
'Voice ATS (FGCOM) on Multi Player mode' => "Voix ATS (FGCOM) en mode multi-Joueurs",
'FGCOM provides a real voice ATC so you can talk to your wing mates while flying in multiplayer mode.' => "FGCOM offre une voix ATC réelle ce qui vous permet de parler à vos collègues pendant un vol en mode multi-joueurs.",
'Enable FGCOM in Multi Player mode' => "Valider FGCOM en mode multi-Joueurs",
'FGCOM Server' => "FGCOM Serveur",

# Others
'Command-line options' => "Options de ligne de commande",
'Specify space-separated options (e.g. --key1=val1 --key2=val2)' => "Séparer les options par un espace (exp: --key1=val1 --key2=val2)",
'View Options' => "Voir Options",
'Data Add-ons' => "Données supplémentaires",
'Select aircraft / scenery files and/or folders to install' => "Selectionner avion/scénario et/ou dossier à installer",
'Install Add-on Data' => "Installer les données",
'Open data Folder' => "Ouvrir dossier des données",

# Menu Items
# FlightGear
'FlightGear' => "FlightGear",
'About FlightGear Mac OS X' => "A propos de FlightGear Mac OS X",
'Preferences...' => "Préférences...",
'Services' => "Services",
'Hide FlightGear Mac OS X' => "Masquer FlightGear Mac OS X",
'Hide Others' => "Masquer les autres",
'Show All' => "Tout afficher",
'Quit FlightGear Mac OS X' => "Quitter FlightGear Mac OS X",

# Edit
'Edit' => "Editer",
'Undo' => "Annuler",
'Redo' => "Refaire",
'Cut' => "Couper",
'Copy' => "Copier",
'Paste' => "Coller",
'Delete' => "Supprimer",
'Select All' => "Tout sélectionner",

'Window' => "Fenêtre",
'Minimize' => "Réduire/Agrandir",
'Bring All to Front' => "Tout au premier plan",
'Help' => "Aide",
'FlightGear Mac OS X Help' => "Aide FlightGear Mac OS X",

# Favorite Dialog
'Type a name for a set of options.' => "Entrer le nom d'un ensemble d'options.",
'Add' => "Ajouter",
'Cancel' => "Annuler"
}

