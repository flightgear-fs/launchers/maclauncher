#
#  OptionParser.rb
#  FlightGearOSX
#
#  Created by Tatsuhiro Nishioka on 11/27/06.
#  Copyright (c) 2006 Tatsuhiro Nishioka
#  http://macflightgear.sourceforge.net/
# 
#  This file is part of FlightGearMacOSX.
#
#  FlightGearMacOSX is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  FlightGearMacOSX is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with FlightGearMacOSX. If not, see <http://www.gnu.org/licenses/>
#

require 'osx/cocoa'

#
# Abstract class of Option ; Do not instantiate
#
class Option
  def initialize(name, outlet = nil, default = nil)
    @name = name
    @outlet = outlet
    @value = @initValue = default if (default != nil)
    @default = default
    preferredValue = load()
    @initValue = preferredValue if (preferredValue != nil && preferredValue.to_s != '')
    setInitialValue() if (defined?(setInitialValue))
  end

  def load()
    return Preferences.valueForKey(@name.downcase)
  end
  
  def save()
    Preferences.setValue(@name.downcase, @value)
  end

  def reload()
    # override this method at a subclass if you need to reload data
  end

  def setName(name)
    @name = name
  end

  def rootDir
    dir=NSBundle.mainBundle.resourcePath.to_s
    if (dir == "/usr/bin")
      return File.dirname(__FILE__)
    else
      return dir
    end
  end
  
  def keyName()
    if (defined?(key) && key)
      return key
    else
      return @name.downcase()
    end
  end

  def setValue(value)
    @value = value
  end

  def generate()
    save()
    if (@value && @value != @default)
      if (@value =~ / /) 
        val = '"'+ @value + '"'
      else
        val = @value
      end
      return "--#{keyName}=#{val}"
    else
      return ''
    end
  end
  
  attr_reader :value
end

# 
# CheckBox
# generates "--{enable,disable}-postfix"
# only if non-default value is specified
#
class BoolOption < Option
  # 
  # initialize(name, outlet, postfix, default)
  # This class uses name as postfix if postfix is nil
  # 
  def initialize(name, outlet, postfix=nil, default=false)
    @postfix = (postfix)? postfix : name.to_s.downcase
    @initValue = default
    @default = default
    super(name, outlet, default)
  end

  def key()
    return @postfix
  end

  def setInitialValue()
    @outlet.setState(@initValue) if (@outlet)
  end

  def generate()
    save()
    if (@value != nil && @value != @default && key != "")
      return "--" + ((@value == true)? "enable" : "disable") + "-#{key}"
    else
      return ''
    end
  end

  def setValue(value)
    super(value)
    @outlet.setState(@value) if (@outlet)
  end
  
  def update()
    @value = (@outlet.state().to_i == 1) if (@outlet)
  end
end

# TextField
class ValueOption < Option
  def setInitialValue()
    if (@outlet)
      @outlet.setStringValue(@initValue) if (defined?(@initValue) && @initValue != nil)
    end
  end

  def setValue(value)
    super(value)
    @outlet.setStringValue(@value) if (@outlet)
  end

  def update()
    @value = @outlet.stringValue().to_s if (@outlet)
  end
end

#
# ComboBox
# generates --key=value
# only if value is not the same as default
# 
class SelectableOption < Option	
  def initialize(name, outlet, items, default)
    @items = items
    super(name, outlet, default)
  end

  def setValue(value)
    super(value)
    @outlet.selectItemWithTitle(NSLocalizedString(@value)) if (@outlet)
  end
  
  def reset()
    @outlet.selectItemAtIndex(0) if (@outlet)
  end
 
  def update()
    if (@outlet)
      index = @outlet.indexOfSelectedItem();
    else
      index = -1;
    end

    if (index >= 0)
      @value = @items[index];
    else
      @value = @default;
    end
  end

  def setInitialValue()
    if (@outlet)
      @outlet.removeAllItems()
      @items.each {|item| @outlet.addItemWithTitle(NSLocalizedString(item)) }
    end
    if (defined?(@initValue))
      @value=@initValue
      @outlet.selectItemWithTitle(NSLocalizedString(@value)) if (@outlet)
    end
  end

  def to_a()
    return @items
  end
end

#
# ComboBox
# generates --name-value
# only if non-default value is specified
# 
class SwitchableOption < SelectableOption
  def generate()
    save()
    return '' if (@value == @default)
    return "--#{@name.to_s}-#{@value}"
  end	
end

#
# Slider
# generates --name=value
# only if non-default value is selected
#
class SliderOption < Option
  def initialize(name, outlet = nil, range=[0, 10], default = 0)
    if (outlet)
      outlet.setMinValue(range[0])
      outlet.setMaxValue(range[1])
      outlet.setNumberOfTickMarks(range[1]-range[0])
    end
    super(name, outlet, default)
  end

  def setInitialValue()
    @outlet.setIntValue(@initValue) if (@outlet)
  end

  def update()
    @value = @outlet.intValue().to_i if (@outlet)
  end
end

#
# Option Parser that creates/generates options for launching fgfs
#
class OptionParser
  def initialize(responder, bindings)
    @bindings = bindings

    @options = {}
    @bindings.each do |name, args|
      if (args.instance_of?(Array))
        klass = args.shift 
      else
        klass = args
        args=[]
      end

      begin
        @options[name.to_s] = klass.new(name.to_s, responder.outlet(name), *args)
      rescue NameError
        @options[name.to_s] = klass.new(name.to_s, nil, *args)
      end
    end
  end
  
  def parse()
    args = []
    @options.values.each {|option| option.update() }
    @options.values.each {|option| args << option.generate() }
    return args.delete_if {|arg| arg == ''}
  end

  def update(name)
    @options[name.to_s].update()
  end

  def reload(name)
    @options[name.to_s].reload()
  end
  
  attr_reader :options
end

module OptionHolder
  def getOutlet(name)
    return @outlets[name] if (defined?(@outlets))
    return nil
  end
  
  def outlet(name)
    variable = instance_variable_get("@#{name}") 
    return variable if (variable)
    return getOutlet(name)
  end
end

