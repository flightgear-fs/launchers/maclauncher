#
#  SearchableOptions.rb
#  FlightGearOSX
#
#  Created by Tatsuhiro Nishioka on 01/09/08.
#  Copyright (c) 2008 Tatsuhiro Nishioka.
#  http://macflightgear.sourceforge.net/
# 
#  This file is part of FlightGearMacOSX.
#
#  FlightGearMacOSX is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  FlightGearMacOSX is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with FlightGearMacOSX. If not, see <http://www.gnu.org/licenses/>
#

require 'osx/cocoa'

include OSX

#
# Base class for Options with TableView and SearchBox
#
class SearchableOptions < NSObject
  ib_outlets :table, :searchBox, :entry
  
  def awakeFromNib()
    @table.setDataSource(@dictionary)
    notifier = OSX::NSNotificationCenter.defaultCenter
    {'optionsWillTerminate:' => OSX::NSApplicationWillTerminateNotification,
      'synchronize:' => OSX::NSWindowDidBecomeKeyNotification
    }.each do |selector, name|
      notifier.addObserver_selector_name_object(self, selector, name, nil)
    end
  end
  
  def setEntry(sender=nil)
    return false if (!@table || @table.selectedRow.to_i < 0)
    searchResult = @dictionary.searchResult
    @selected = searchResult[@table.selectedRow.to_i]
    if (@entry)
      @entry.setStringValue(@selected.stringValue())
      return true
    else
      return false
    end
  end

  def synchronize(sender = nil)
    NSNotificationCenter.defaultCenter.removeObserver_name_object(self, OSX::NSWindowDidBecomeKeyNotification, nil)
    return true
  end

  def search(sender=nil)
    keyword = @searchBox.stringValue().to_s
    if (defined?(yield))
      yield(keyword)
    else
      @dictionary.search(keyword)
    end
    @table.reloadData()
  end

  def optionsWillTerminate(notification=nil)
    NSNotificationCenter.defaultCenter.removeObserver(self)
  end

end
