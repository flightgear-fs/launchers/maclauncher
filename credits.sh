#!/bin/sh
cat << HTMLEND
<html>
<body>
<center>
<font color=blue>FlightGear - Free Flight Simulator</font><br>
</center>

<font size=3>
Built by $USER on `date +'%Y-%m-%d'`, using:<br>
&nbsp; <a href=http://gitorious.org/fg/flightgear>FlightGear.git</a> : `cd flightgear/FlightGear && git branch | grep "*" | cut -d " " -f 2`, `cd flightGear/FlightGear && git show | grep Date: | cut -d ' ' -f 5,6,8` <br>
&nbsp; <a href=http://gitorious.org/fg/simgear>SimGear.git</a> : `cd SimGear/SimGear && git branch | grep "*" | cut -d " " -f 2`, `cd SimGear/SimGear && git show | grep Date: | cut -d ' ' -f 5,6,8` <br>
&nbsp; <a href=http://gitorious.org/fg/fgdata>fgdata.git</a> : `cd FlightGearOSX/data && git branch | grep "*" | cut -d " " -f 2`, `cd FlightGearOSX/data && git show | grep Date: | cut -d ' ' -f 5,6,8` <br>
&nbsp; <a href=http://macflightgear.svn.sourceforge.net/viewvc/macflightgear/>GUI Launcher and builder by tat</a>: rev $REV
<br>
</font>
<br>
<font face="Helvetica" size="3" color="purple">
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <a href=http://macflightgear.sourceforge.net>FlightGear Mac OS X Project</font></a>
</font>
</body>
</html>
HTMLEND
